/*
 * main.cpp
 *
 *  Created on: Nov 24, 2016
 *      Author: root
 */

#include <iostream>
#include <string>
#include <fstream>
#include <memory>
#include "gtest/gtest.h"
#include "Boxes/IsoBoxes.h"
#include "IsoParser.h"
#include "Util.h"

int main(int argc, char* argv[]) {

	testing::InitGoogleTest(&argc, argv);

	int returnCode = RUN_ALL_TESTS();

	return returnCode;
}



