/*
 * IsoTreeTests.cpp
 *
 *  Created on: Nov 24, 2016
 *      Author: root
 */
#include <memory>
#include "gtest/gtest.h"
#include "IsoTree.h"
#include "Boxes/IsoBoxes.h"
#include "ByteBuffer.h"
#include "IsoTreeTester.h"

TEST_F(IsoTreeTester, ParseTest) {

	ByteBuffer serializationBuffer;
	m_isoTree->ToByteBuffer(serializationBuffer);

	ASSERT_TRUE(serializationBuffer == this->m_expectedBuffer);
}

TEST_F(IsoTreeTester, FindBoxTest) {

	m_isoTree->Print();

	auto findResults = m_isoTree->FindBox("stts");

	ASSERT_TRUE(2 == findResults.size());
}

TEST_F(IsoTreeTester, FindBoxHierarchyTest) {

	std::vector<std::string> boxChain = {"trak", "mdia", "minf", "stbl", "stsd", "avc1"};

	auto findResults = m_isoTree->FindBoxChain(boxChain);

	ASSERT_TRUE(1 == findResults.size());
}

