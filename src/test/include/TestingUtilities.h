/*
 * TestingUtilities.h
 *
 *  Created on: Nov 24, 2016
 *      Author: root
 */

#ifndef SRC_TEST_INCLUDE_TESTINGUTILITIES_H_
#define SRC_TEST_INCLUDE_TESTINGUTILITIES_H_

class TestingUtilities
{
public:
	static void ReadFileToByteBuffer(const char* filepath, ByteBuffer& buffer) {

		std::ifstream ifs(filepath, std::ios::binary | std::ios::in);

		if (ifs.is_open()) {
			ifs.seekg(0, std::ios::end);
			size_t bufferSize = ifs.tellg();
			ifs.seekg(0, std::ios::beg);

			buffer.Reserve(bufferSize + 1);

			buffer.Append(ifs, bufferSize);

			ifs.close();
		}
	}
};

#endif /* SRC_TEST_INCLUDE_TESTINGUTILITIES_H_ */
