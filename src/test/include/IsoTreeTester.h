/*
 * IsoTreeTester.h
 *
 *  Created on: Nov 24, 2016
 *      Author: root
 */

#ifndef SRC_TEST_INCLUDE_ISOTREETESTER_H_
#define SRC_TEST_INCLUDE_ISOTREETESTER_H_

#include <memory>
#include "IsoParser.h"
#include "TestingUtilities.h"

class IsoTreeTester : public testing::Test
{
private:
	const char* FILEPATH = "/root/workspace_neon_1/ISO_BASE_MEDIA_FORMAT_PARSER/resources/SampleVideo_1280x720_5mb.mp4";
public:
	IsoTreeTester() : testing::Test() {
		m_isoTree = IsoParser::Parse(this->GetFilePath());
		TestingUtilities::ReadFileToByteBuffer(this->GetFilePath(), m_expectedBuffer);
	}

	const char* GetFilePath() {
		return this->FILEPATH;
	}
protected:
	std::shared_ptr<IsoTree> 	m_isoTree;
	ByteBuffer 					m_expectedBuffer;
};



#endif /* SRC_TEST_INCLUDE_ISOTREETESTER_H_ */
