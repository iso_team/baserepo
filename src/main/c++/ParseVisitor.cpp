/*
 * ParseVisitor.cpp
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */
#include "Visitors/ParseVisitor.h"
#include "Boxes/IsoBoxes.h"

/// ctor
/// \param[in,out] rootBox Shared ptr to the root of IsoTree
/// \param[in] byteBuffer Reference to a raw data
/// \param[in] dataOffset Parsing pivot offset
ParseVisitor::ParseVisitor(std::shared_ptr<ISO_BASE_COMPOSITE_BOX> rootBox, ByteBuffer& byteBuffer, uint64_t dataOffset) :
		m_refData(byteBuffer), m_dataOffset(dataOffset), m_currentBox_Offset(0), m_currentBox_Size(8) {
	m_compositeBoxes.push(rootBox);
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_FTYP());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_MVHD());
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_MOOV());
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_TRAK());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_TKHD());
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_MDIA());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_MDHD());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_HDLR());
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_MINF());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_VMHD());
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_DINF());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_DREF());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_URL());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_URN());
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_STBL());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_STSD());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_AVC1());
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_AVCC());
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_BTRT());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_STTS());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_STSS());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_STSC());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_STSZ());
	m_isoBoxesParsingFragments.push_back(new ISO_FULLBOX_STCO());

	// This one needs to be last in list => Used when no box has been defined for the currently processed box
	m_isoBoxesParsingFragments.push_back(new ISO_BASE_BOX_UNKNOWN());
}

/// dtor
ParseVisitor::~ParseVisitor() {
	size_t parsingFragmentsCount = m_isoBoxesParsingFragments.size();
	for (size_t i = 0; i < parsingFragmentsCount; i++) {
		if (m_isoBoxesParsingFragments[i] != nullptr) {
			delete m_isoBoxesParsingFragments[i];
			m_isoBoxesParsingFragments[i] = nullptr;
		}
	}
}

/// Push newly created parent box to the top of a parent stack
void ParseVisitor::PushParentBox(std::shared_ptr<ICOMPOSITE_BOX> sptrParentBox) {
	m_compositeBoxes.push(sptrParentBox);
}

/// Pop parent box after all children were added
void ParseVisitor::PopParentBox() {
	return m_compositeBoxes.pop();
}

/// In the parsing procedure we need to know name of a box that shall be parsed next
/// This method is used for fetching this info
std::string ParseVisitor::GetNextBoxNameParentBox() {
	std::string nextBoxName;
	if (m_dataOffset + 8 <= m_refData.GetSize()) {
		nextBoxName.assign(reinterpret_cast<char*>(m_refData.DataPtr() + m_dataOffset + sizeof(uint32_t)), sizeof(uint32_t));
	} else {
		nextBoxName.clear();
	}
	return nextBoxName;
}

/// Used throughout parsing procedure to keep track if we have valid offset
bool ParseVisitor::AreOffsetsInValidRange(uint32_t sizeOfTheDataToRead) {
	if (this->m_dataOffset + sizeOfTheDataToRead <= this->m_currentBox_Offset + this->m_currentBox_Size) {
		return true;
	}
	return false;
}

/// Commence double dispatch
void ParseVisitor::AcceptAll() {
	size_t parsingFragmentsCount = this->m_isoBoxesParsingFragments.size();

	for (size_t i = 0; (i < parsingFragmentsCount) && (this->m_dataOffset < this->m_refData.GetSize()); i++) {
		this->m_isoBoxesParsingFragments[i]->Accept(*this);
	}
}

/// Used throughout parsing procedure to keep track if all data has been parsed
bool ParseVisitor::IsDataAvailable() {
	if (this->m_dataOffset < this->m_refData.GetSize()) {
		return true;
	}
	return false;
}

/// Parse procedure for a Composite base box
void ParseVisitor::Parse(std::shared_ptr<ISO_BASE_COMPOSITE_BOX> sptrNewComposite) {

	this->PushParentBox(sptrNewComposite);

	uint64_t startOffset = sptrNewComposite->GetStartOffset();
	size_t parsingFragmentsCount = (size_t) this->m_isoBoxesParsingFragments.size();
	while ((startOffset - 2 * sizeof(uint32_t) + sptrNewComposite->m_boxSize > this->m_dataOffset)) {
		for (size_t i = 0; (i < parsingFragmentsCount) && (startOffset - 2 * sizeof(uint32_t) + sptrNewComposite->m_boxSize > this->m_dataOffset); i++) {
			this->m_isoBoxesParsingFragments[i]->Accept(*this);
		}
	}

	this->PopParentBox();
	this->m_compositeBoxes.top()->AddChild(sptrNewComposite);
}

/// Parse procedure for a Composite Fullbox
void ParseVisitor::Parse(std::shared_ptr<ISO_COMPOSITE_FULLBOX> sptrNewComposite) {

	this->PushParentBox(sptrNewComposite);

	uint64_t startOffset = sptrNewComposite->GetStartOffset();
	size_t parsingFragmentsCount = (size_t) this->m_isoBoxesParsingFragments.size();
	while ((startOffset - 2 * sizeof(uint32_t) + sptrNewComposite->m_boxSize > this->m_dataOffset)) {
		for (size_t i = 0; (i < parsingFragmentsCount) && (startOffset - 2 * sizeof(uint32_t) + sptrNewComposite->m_boxSize > this->m_dataOffset); i++) {
			this->m_isoBoxesParsingFragments[i]->Accept(*this);
		}
	}
	this->PopParentBox();
	this->m_compositeBoxes.top()->AddChild(sptrNewComposite);
}

void ParseVisitor::visit(ISO_BASE_BOX* box) {
	this->m_currentBox_Offset = this->m_dataOffset;
	this->ReadInteger(box->m_boxSize);
	this->m_currentBox_Size = box->m_boxSize;
	this->ReadString(box->m_boxName, sizeof(uint32_t));
}

void ParseVisitor::visit(ISO_FULLBOX* box) {
	box->ISO_BASE_BOX::Accept(*this);
	this->ReadInteger(box->m_versionAndFlags);
}

void ParseVisitor::visit(ICOMPOSITE_BOX* box) {
	box->ISO_BASE_BOX::Accept(*this);

	auto children = box->GetChildren();
	uint32_t childrenCount = static_cast<uint32_t>(children.size());
	for (size_t i = 0; i < childrenCount; i++) {
		children[i]->Accept(*this);
	}
}

void ParseVisitor::visit(ISO_BASE_COMPOSITE_BOX* box) {
	box->ISO_BASE_BOX::Accept(*this);
}

void ParseVisitor::visit(ISO_COMPOSITE_FULLBOX* box) {
	box->ICOMPOSITE_BOX::Accept(*this);
}

void ParseVisitor::visit(ISO_BASE_BOX_UNKNOWN* box) {

	box->ISO_BASE_BOX::Accept(*this);

	auto sptrUnknownBox = std::make_shared<ISO_BASE_BOX_UNKNOWN>(box->m_boxName, this->m_compositeBoxes.top().get(), box->m_boxSize, this->m_currentBox_Offset,
			this->m_compositeBoxes.top()->GetBoxLevel() + 1);

	this->ReadString(sptrUnknownBox->m_byteBuffer, box->m_boxSize - 2 * sizeof(uint32_t));

	this->m_compositeBoxes.top()->AddChild(sptrUnknownBox);
}

void ParseVisitor::visit(ISO_BASE_BOX_FTYP* box) {

	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {

		box->ISO_BASE_BOX::Accept(*this);

		auto sptrFtypBox = std::make_shared<ISO_BASE_BOX_FTYP>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrFtypBox->m_majorVersion);
		this->ReadInteger(sptrFtypBox->m_minorVersion);

		size_t compatibleBrandCount = (box->m_boxSize - 4 * sizeof(uint32_t)) / sizeof(uint32_t);

		sptrFtypBox->m_compatibleBrands.assign(compatibleBrandCount, 0);

		for (size_t i = 0; i < compatibleBrandCount; i++) {
			this->ReadInteger(sptrFtypBox->m_compatibleBrands[i]);
		}

		this->m_compositeBoxes.top()->AddChild(sptrFtypBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_MVHD* box) {

	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_FULLBOX::Accept(*this);

		std::shared_ptr<ISO_FULLBOX_MVHD> sptrNewMvhdBox;
		if ((box->m_versionAndFlags >> 24) == 0) {
			ISO_FULLBOX_MVHD_32BIT* mvhd32Box = new ISO_FULLBOX_MVHD_32BIT(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
					this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);
			this->ReadInteger(mvhd32Box->m_creationTime);
			this->ReadInteger(mvhd32Box->m_modificationTime);
			this->ReadInteger(mvhd32Box->m_timescale);
			this->ReadInteger(mvhd32Box->m_duration);
			sptrNewMvhdBox.reset(mvhd32Box);
		} else if ((box->m_versionAndFlags >> 24) == 1) {
			ISO_FULLBOX_MVHD_64BIT* mvhd64Box = new ISO_FULLBOX_MVHD_64BIT(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
					this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);
			this->ReadInteger(mvhd64Box->m_creationTime);
			this->ReadInteger(mvhd64Box->m_modificationTime);
			this->ReadInteger(mvhd64Box->m_timescale);
			this->ReadInteger(mvhd64Box->m_duration);
			sptrNewMvhdBox.reset(mvhd64Box);
		} else {
			return;
		}

		this->ReadInteger(sptrNewMvhdBox->m_rate);
		this->ReadInteger(sptrNewMvhdBox->m_volume);
		this->ReadIntegerArray(sptrNewMvhdBox->m_reserved);
		this->ReadInteger(sptrNewMvhdBox->m_nextTrackId);

		this->m_compositeBoxes.top()->AddChild(sptrNewMvhdBox);
	}
}

void ParseVisitor::visit(ISO_BASE_BOX_MOOV* box) {

	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {

		box->ISO_BASE_BOX::Accept(*this);

		std::shared_ptr<ISO_BASE_BOX_MOOV> sptrNewMoovBox = std::make_shared<ISO_BASE_BOX_MOOV>(box->m_boxSize, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->Parse(sptrNewMoovBox);
	}
}

void ParseVisitor::visit(ISO_BASE_BOX_TRAK* box) {

	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {

		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewTrakBox = std::make_shared<ISO_BASE_BOX_TRAK>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->Parse(sptrNewTrakBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_TKHD* box) {

	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {

		box->ISO_FULLBOX::Accept(*this);

		std::shared_ptr<ISO_FULLBOX_TKHD> sptrNewTkhdBox;
		if ((box->m_versionAndFlags >> 24) == 0) {
			ISO_FULLBOX_TKHD_32BIT* tkhd32Box = new ISO_FULLBOX_TKHD_32BIT(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
					this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);
			this->ReadInteger(tkhd32Box->m_creationTime);
			this->ReadInteger(tkhd32Box->m_modificationTime);
			this->ReadInteger(tkhd32Box->m_trackID);
			this->ReadInteger(tkhd32Box->m_reserved);
			this->ReadInteger(tkhd32Box->m_duration);
			sptrNewTkhdBox.reset(tkhd32Box);
		} else if ((box->m_versionAndFlags >> 24) == 1) {
			ISO_FULLBOX_TKHD_64BIT* tkhd64Box = new ISO_FULLBOX_TKHD_64BIT(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
					this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);
			this->ReadInteger(tkhd64Box->m_creationTime);
			this->ReadInteger(tkhd64Box->m_modificationTime);
			this->ReadInteger(tkhd64Box->m_trackID);
			this->ReadInteger(tkhd64Box->m_reserved);
			this->ReadInteger(tkhd64Box->m_duration);
			sptrNewTkhdBox.reset(tkhd64Box);
		} else {
			return;
		}

		this->ReadIntegerArray(sptrNewTkhdBox->m_reservedStart);
		this->ReadInteger(sptrNewTkhdBox->m_layer);
		this->ReadInteger(sptrNewTkhdBox->m_alternateGroup);
		this->ReadInteger(sptrNewTkhdBox->m_volume);
		this->ReadIntegerArray(sptrNewTkhdBox->m_reservedEnd);
		this->ReadInteger(sptrNewTkhdBox->m_width);
		this->ReadInteger(sptrNewTkhdBox->m_height);

		this->m_compositeBoxes.top()->AddChild(sptrNewTkhdBox);
	}
}

void ParseVisitor::visit(ISO_BASE_BOX_MDIA* box) {

	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {

		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewTrakBox = std::make_shared<ISO_BASE_BOX_MDIA>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->Parse(sptrNewTrakBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_MDHD* box) {

	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {

		box->ISO_FULLBOX::Accept(*this);

		std::shared_ptr<ISO_FULLBOX_MDHD> sptrNewMdhdBox;

		if ((box->m_versionAndFlags >> 24) == 0) {
			ISO_FULLBOX_MDHD_32BIT* tkhd32Box = new ISO_FULLBOX_MDHD_32BIT(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
					this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);
			this->ReadInteger(tkhd32Box->m_creationTime);
			this->ReadInteger(tkhd32Box->m_modificationTime);
			this->ReadInteger(tkhd32Box->m_timescale);
			this->ReadInteger(tkhd32Box->m_duration);
			sptrNewMdhdBox.reset(tkhd32Box);
		} else if ((box->m_versionAndFlags >> 24) == 1) {
			ISO_FULLBOX_MDHD_64BIT* tkhd64Box = new ISO_FULLBOX_MDHD_64BIT(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
					this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);
			this->ReadInteger(tkhd64Box->m_creationTime);
			this->ReadInteger(tkhd64Box->m_modificationTime);
			this->ReadInteger(tkhd64Box->m_timescale);
			this->ReadInteger(tkhd64Box->m_duration);
			sptrNewMdhdBox.reset(tkhd64Box);
		} else {
			return;
		}

		this->ReadInteger(sptrNewMdhdBox->m_language);
		this->ReadInteger(sptrNewMdhdBox->m_predefined);

		this->m_compositeBoxes.top()->AddChild(sptrNewMdhdBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_HDLR* box) {

	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {

		box->ISO_FULLBOX::Accept(*this);

		auto sptrNewHdlrBox = std::make_shared<ISO_FULLBOX_HDLR>(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewHdlrBox->m_predefined);
		this->ReadInteger(sptrNewHdlrBox->m_handlerType);
		this->ReadIntegerArray(sptrNewHdlrBox->m_reserved);
		this->ReadTerminatedString(sptrNewHdlrBox->m_name);

		this->m_compositeBoxes.top()->AddChild(sptrNewHdlrBox);
	}
}

void ParseVisitor::visit(ISO_BASE_BOX_MINF* box) {

	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewTrakBox = std::make_shared<ISO_BASE_BOX_MINF>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->Parse(sptrNewTrakBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_VMHD* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_FULLBOX::Accept(*this);

		auto sptrNewVmhdBox = std::make_shared<ISO_FULLBOX_VMHD>(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewVmhdBox->m_graphicsMode);

		this->ReadIntegerArray(sptrNewVmhdBox->m_opColor);

		this->m_compositeBoxes.top()->AddChild(sptrNewVmhdBox);
	}
}

void ParseVisitor::visit(ISO_BASE_BOX_DINF* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewDinfBox = std::make_shared<ISO_BASE_BOX_DINF>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->Parse(sptrNewDinfBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_DREF* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewDrefBox = std::make_shared<ISO_FULLBOX_DREF>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewDrefBox->m_versionAndFlags);
		this->ReadInteger(sptrNewDrefBox->m_entryCount);

		this->Parse(sptrNewDrefBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_URL* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_FULLBOX::Accept(*this);

		auto sptrNewUrlBox = std::make_shared<ISO_FULLBOX_URL>(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		if ((sptrNewUrlBox->m_versionAndFlags >> 24) == 1) {
			this->ReadTerminatedString(sptrNewUrlBox->m_location);
		}

		this->m_compositeBoxes.top()->AddChild(sptrNewUrlBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_URN* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_FULLBOX::Accept(*this);

		auto sptrNewUrnBox = std::make_shared<ISO_FULLBOX_URN>(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		if ((sptrNewUrnBox->m_versionAndFlags >> 24) == 1) {
			this->ReadTerminatedString(sptrNewUrnBox->m_name);
			this->ReadTerminatedString(sptrNewUrnBox->m_location);
		}
		this->m_compositeBoxes.top()->AddChild(sptrNewUrnBox);
	}
}

void ParseVisitor::visit(ISO_BASE_BOX_STBL* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewStblBox = std::make_shared<ISO_BASE_BOX_STBL>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->Parse(sptrNewStblBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_STSD* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewStsdBox = std::make_shared<ISO_FULLBOX_STSD>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewStsdBox->m_versionAndFlags);
		this->ReadInteger(sptrNewStsdBox->m_numEntries);

		this->Parse(sptrNewStsdBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_AVC1* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewAvc1Box = std::make_shared<ISO_FULLBOX_AVC1>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewAvc1Box->m_versionAndFlags);
		this->ReadIntegerArray(sptrNewAvc1Box->m_reservedStart);
		this->ReadInteger(sptrNewAvc1Box->m_dataRefIndex);
		this->ReadIntegerArray(sptrNewAvc1Box->m_reservedEnd);
		this->ReadInteger(sptrNewAvc1Box->m_width);
		this->ReadInteger(sptrNewAvc1Box->m_height);
		this->ReadInteger(sptrNewAvc1Box->m_horRes);
		this->ReadInteger(sptrNewAvc1Box->m_unknwnHorValue);
		this->ReadInteger(sptrNewAvc1Box->m_vertRes);
		this->ReadIntegerArray(sptrNewAvc1Box->m_compressorName);
		this->ReadInteger(sptrNewAvc1Box->m_colorDepth);
		this->ReadInteger(sptrNewAvc1Box->m_predefined);

		this->Parse(sptrNewAvc1Box);
	}
}

void ParseVisitor::visit(ISO_BASE_BOX_AVCC* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewAvccBox = std::make_shared<ISO_BASE_BOX_AVCC>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewAvccBox->m_configVersion);
		this->ReadInteger(sptrNewAvccBox->m_avcProfileIndication);
		this->ReadInteger(sptrNewAvccBox->m_profileCompatibility);
		this->ReadInteger(sptrNewAvccBox->m_avcLevelIndication);

		this->ReadInteger(sptrNewAvccBox->m_length);
		sptrNewAvccBox->m_length = (sptrNewAvccBox->m_length & 0x03) + 1;

		this->ReadInteger(sptrNewAvccBox->m_spsCount);
		sptrNewAvccBox->m_spsCount &= 0x1F;

		sptrNewAvccBox->m_spsEntries.assign(sptrNewAvccBox->m_spsCount, new ByteBuffer());
		for (size_t i = 0; i < sptrNewAvccBox->m_spsCount; i++) {
			uint16_t spsEntryLen;
			this->ReadInteger(spsEntryLen);
			this->ReadString(*sptrNewAvccBox->m_spsEntries[i], spsEntryLen);
		}

		this->ReadInteger(sptrNewAvccBox->m_ppsCount);
		sptrNewAvccBox->m_ppsEntries.assign(sptrNewAvccBox->m_ppsCount, new ByteBuffer());
		for (size_t i = 0; i < sptrNewAvccBox->m_ppsCount; i++) {
			uint16_t ppsEntryLen;
			this->ReadInteger(ppsEntryLen);
			this->ReadString(*sptrNewAvccBox->m_ppsEntries[i], ppsEntryLen);
		}

		this->m_compositeBoxes.top()->AddChild(sptrNewAvccBox);
	}
}

void ParseVisitor::visit(ISO_BASE_BOX_BTRT* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_BASE_BOX::Accept(*this);

		auto sptrNewBtrtBox = std::make_shared<ISO_BASE_BOX_BTRT>(box->m_boxSize, this->m_compositeBoxes.top().get(), this->m_currentBox_Offset,
				this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewBtrtBox->m_bufferSize);
		this->ReadInteger(sptrNewBtrtBox->m_maxBitrate);
		this->ReadInteger(sptrNewBtrtBox->m_minBitrate);

		this->m_compositeBoxes.top()->AddChild(sptrNewBtrtBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_STTS* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_FULLBOX::Accept(*this);

		auto sptrNewSttsBox = std::make_shared<ISO_FULLBOX_STTS>(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewSttsBox->m_entryCount);
		sptrNewSttsBox->AllocateEntries(sptrNewSttsBox->m_entryCount);

		for (size_t i = 0; i < sptrNewSttsBox->m_entryCount; i++) {
			this->ReadInteger(sptrNewSttsBox->m_sttsEntries[i].m_sampleCount);
			this->ReadInteger(sptrNewSttsBox->m_sttsEntries[i].m_sampleDelta);
		}

		this->m_compositeBoxes.top()->AddChild(sptrNewSttsBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_STSS* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_FULLBOX::Accept(*this);

		auto sptrNewStssBox = std::make_shared<ISO_FULLBOX_STSS>(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewStssBox->m_entryCount);
		sptrNewStssBox->m_stssEntries.assign(sptrNewStssBox->m_entryCount, 0);

		for (size_t i = 0; i < sptrNewStssBox->m_entryCount; i++) {
			this->ReadInteger(sptrNewStssBox->m_stssEntries[i]);
		}

		this->m_compositeBoxes.top()->AddChild(sptrNewStssBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_STSC* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_FULLBOX::Accept(*this);

		auto sptrNewStscBox = std::make_shared<ISO_FULLBOX_STSC>(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewStscBox->m_entryCount);
		sptrNewStscBox->AllocateEntries(sptrNewStscBox->m_entryCount);

		for (size_t i = 0; i < sptrNewStscBox->m_entryCount; i++) {
			this->ReadInteger(sptrNewStscBox->m_stscEntries[i].m_firstChunk);
			this->ReadInteger(sptrNewStscBox->m_stscEntries[i].m_samplesPerChunk);
			this->ReadInteger(sptrNewStscBox->m_stscEntries[i].m_sampleDescIndex);
		}

		this->m_compositeBoxes.top()->AddChild(sptrNewStscBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_STSZ* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_FULLBOX::Accept(*this);

		auto sptrNewStszBox = std::make_shared<ISO_FULLBOX_STSZ>(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewStszBox->m_sampleSize);
		this->ReadInteger(sptrNewStszBox->m_sampleCount);

		sptrNewStszBox->m_entrySizes.assign(sptrNewStszBox->m_sampleCount, 0);

		if (sptrNewStszBox->m_sampleSize == 0) {
			for (size_t i = 0; i < sptrNewStszBox->m_sampleCount; i++) {
				this->ReadInteger(sptrNewStszBox->m_entrySizes[i]);
			}
		}

		this->m_compositeBoxes.top()->AddChild(sptrNewStszBox);
	}
}

void ParseVisitor::visit(ISO_FULLBOX_STCO* box) {
	if ((this->GetNextBoxNameParentBox() == box->GetBoxName()) && this->IsDataAvailable()) {
		box->ISO_FULLBOX::Accept(*this);

		auto sptrNewStcoBox = std::make_shared<ISO_FULLBOX_STCO>(box->m_boxSize, box->m_versionAndFlags, this->m_compositeBoxes.top().get(),
				this->m_currentBox_Offset, this->m_compositeBoxes.top()->GetBoxLevel() + 1);

		this->ReadInteger(sptrNewStcoBox->m_entryCount);
		sptrNewStcoBox->m_chunkOffsets.assign(sptrNewStcoBox->m_entryCount, 0);

		for (size_t i = 0; i < sptrNewStcoBox->m_entryCount; i++) {
			this->ReadInteger(sptrNewStcoBox->m_chunkOffsets[i]);
		}

		this->m_compositeBoxes.top()->AddChild(sptrNewStcoBox);
	}
}

