/*
 * PrintVisitor.cpp
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */
#include "Visitors/PrintVisitor.h"
#include "Boxes/IsoBoxes.h"

//////////////////////////
/// PRINT VISITOR IMPL ///
//////////////////////////
void PrintVisitor::visit(ISO_BASE_BOX* box)				{ box->Print(); }
void PrintVisitor::visit(ISO_FULLBOX* box)				{ box->ISO_BASE_BOX::Accept(*this); }

void PrintVisitor::visit(ICOMPOSITE_BOX* box)			{
	box->ISO_BASE_BOX::Accept(*this);

	auto children = box->GetChildren();
	uint32_t childrenCount = static_cast<uint32_t>(children.size());
	for (size_t i = 0; i < childrenCount; i++) {
		children[i]->Accept(*this);
	}
}

void PrintVisitor::visit(ISO_BASE_COMPOSITE_BOX* box)	{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_COMPOSITE_FULLBOX* box)	{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_UNKNOWN* box)		{ std::cout << "UNK"; box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_FTYP* box)		{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_MVHD* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_MOOV* box)		{ box->ISO_BASE_COMPOSITE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_TRAK* box)		{ box->ISO_BASE_COMPOSITE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_TKHD* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_MDIA* box)		{ box->ISO_BASE_COMPOSITE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_MDHD* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_HDLR* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_MINF* box)		{ box->ISO_BASE_COMPOSITE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_VMHD* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_DINF* box)		{ box->ISO_BASE_COMPOSITE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_DREF* box)			{ box->ISO_COMPOSITE_FULLBOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_URL* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_URN* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_STBL* box)		{ box->ISO_BASE_COMPOSITE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_STSD* box)			{ box->ISO_COMPOSITE_FULLBOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_AVC1* box)			{ box->ISO_COMPOSITE_FULLBOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_AVCC* box)		{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_BASE_BOX_BTRT* box)		{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_STTS* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_STSS* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_STSC* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_STSZ* box)			{ box->ISO_BASE_BOX::Accept(*this); }
void PrintVisitor::visit(ISO_FULLBOX_STCO* box)			{ box->ISO_BASE_BOX::Accept(*this); }





