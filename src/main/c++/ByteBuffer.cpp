/*
 * ByteBuffer.cpp
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */
#include "ByteBuffer.h"
#include <cstring>
#include <algorithm>

/// ctor
ByteBuffer::ByteBuffer() {
	this->m_ptrBuffer = new unsigned char[128];
	this->m_capacity = 128;
	this->m_usedCapacity = 0;
}

/// Parameterized ctor
ByteBuffer::ByteBuffer(unsigned long capacity) {
	this->m_ptrBuffer = new unsigned char[capacity];
	this->m_capacity = capacity;
	this->m_usedCapacity = 0;
}

/// copy ctor
ByteBuffer::ByteBuffer(const ByteBuffer& rhs) {

	this->m_ptrBuffer = new unsigned char[2 * rhs.m_usedCapacity];
	this->m_capacity = 2 * rhs.m_usedCapacity;
	this->m_usedCapacity = rhs.m_usedCapacity;

	::memcpy(this->m_ptrBuffer, rhs.m_ptrBuffer, rhs.m_usedCapacity);
}

/// dtor
ByteBuffer::~ByteBuffer() {
	if (this->m_ptrBuffer) {
		delete[] this->m_ptrBuffer;
		this->m_ptrBuffer = NULL;
	}
}

/// assignment operator
ByteBuffer& ByteBuffer::operator=(const ByteBuffer& rhs) {

	if(this == &rhs) {
		return *this;
	}
	delete [] this->m_ptrBuffer;
	this->m_ptrBuffer = new unsigned char[2*rhs.m_usedCapacity];
	this->m_capacity = 2 * rhs.m_usedCapacity;
	this->m_usedCapacity = rhs.m_usedCapacity;

	::memcpy(this->m_ptrBuffer, rhs.m_ptrBuffer, rhs.m_usedCapacity);

	return *this;
}

/// Equality operator
bool ByteBuffer::operator==(const ByteBuffer& rhs) {
	if(this->m_usedCapacity != rhs.m_usedCapacity) {
		return false;
	}
	if(strcmp((const char*)this->m_ptrBuffer, (const char*)rhs.m_ptrBuffer) != 0) {
		return false;
	}
	return true;
}

/// std::assign like operation
/// @param ptrBuffer
/// @param bufferSize
/// @param doSwap
void ByteBuffer::assign(const void* ptrBuffer, size_t bufferSize, bool doSwap) {
	this->Empty();
	this->Append(ptrBuffer, bufferSize, doSwap);
}

/// Append memory chunk
/// @param ptrBuffer
/// @param bufferSize
/// @param doSwap
void ByteBuffer::Append(const void* ptrBuffer, size_t bufferSize, bool doSwap) {
	if (!ptrBuffer || !bufferSize) {
		return;
	}

	if (this->m_usedCapacity + bufferSize <= this->m_capacity) {
		// Here we do not need to consider self-appending
		if (doSwap){
			char* istart = (char*)ptrBuffer;
			char* iend = istart + bufferSize;
			std::reverse(istart, iend);
		}
		::memcpy(this->m_ptrBuffer + this->m_usedCapacity, ptrBuffer, bufferSize);

		if (doSwap){
			char* istart = (char*)ptrBuffer;
			char* iend = istart + bufferSize;
			std::reverse(istart, iend);
		}

		this->m_usedCapacity += static_cast<unsigned long>(bufferSize);
	}
	else {
		// We need reallocation
		bool fSelfAppend = (this->m_ptrBuffer == ptrBuffer);

		Reserve(2 * this->m_capacity + bufferSize);

		if (!fSelfAppend) {
			Append(ptrBuffer, bufferSize, doSwap);
		}
		else {
			// Self-appending, but this time we can stand it
			Append(this->m_ptrBuffer, bufferSize, doSwap);
		}
	}
}

/// Append ByteBuffer
/// @param byteBuffer
/// @param doSwap
void ByteBuffer::Append(const ByteBuffer& byteBuffer, bool doSwap) {
	this->Append((void*)byteBuffer.DataPtr(), byteBuffer.GetSize(), doSwap);
}

///  Append from input file stream N bytes (N = count)
/// @param ifs
/// @param count
void ByteBuffer::Append(std::ifstream& ifs, size_t count) {
	ifs.read((char*)this->m_ptrBuffer, count);
	this->m_usedCapacity = static_cast<unsigned long>(count);
}

/// Reserve more memory
/// @param size
void ByteBuffer::Reserve(size_t size) {
	if (this->m_capacity >= size) {
		return;
	}
	this->m_capacity = static_cast<unsigned long>(size);
	unsigned char* pbNew = new unsigned char[size];
	::memcpy(pbNew, this->m_ptrBuffer, this->m_usedCapacity);
	delete[] this->m_ptrBuffer;
	this->m_ptrBuffer = pbNew;
}

/// Fetch raw ptr to data
/// @return
unsigned char* ByteBuffer::DataPtr() const {
	return this->m_ptrBuffer;
}

/// Fetch data byte count
unsigned long ByteBuffer::GetSize() const {
	return this->m_usedCapacity;
}

/// Clean all
void ByteBuffer::Empty() {
	if (this->m_ptrBuffer) {
		::memset(this->m_ptrBuffer, 0, this->m_capacity);
	}
	this->m_capacity = m_capacity;
	this->m_usedCapacity = 0;
}

/// fetch c-style char array
char* ByteBuffer::c_str() {
	return (char*)this->m_ptrBuffer;
}

/// Set length
/// @param usedCapacity
void ByteBuffer::SetLength(size_t usedCapacity) {
	this->m_usedCapacity = static_cast<unsigned long>(usedCapacity);
}

/// Allocate buffers with capacity
/// @param capacity
void ByteBuffer::Alloc(size_t capacity) {
	this->m_capacity = static_cast<unsigned long>(capacity);
}




