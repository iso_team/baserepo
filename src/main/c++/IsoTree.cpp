/*
 * IsoTree.cpp
 *
 *  Created on: Nov 18, 2016
 *      Author: root
 */

#include "IsoTree.h"
#include "Visitors/FindVisitor.h"
#include "Visitors/PrintVisitor.h"
#include "Visitors/ParseVisitor.h"
#include "Visitors/OutputCreatorVisitor.h"

/// ctor
IsoTree::IsoTree() {
	m_root.reset(new ISO_BASE_COMPOSITE_BOX("root", 0, 0, 0));
}

/// Visit interface
/// @param [in] v Visitor that shall visit tree
void IsoTree::VisitTree(IVisitor& v) {
	m_root->Accept(v);
}

/// Find interface of IsoTree
/// @param [in] boxNames Vector of boxes that shall be searched after
/// @return Vector of found boxes wrapped with shared_ptr
std::vector<std::shared_ptr<ISO_BASE_BOX>> IsoTree::FindBoxChain(std::vector<std::string> boxNames) {

	FindVisitor finder(boxNames);

	this->m_root->Accept(finder);

	return finder.GetResults();
}

/// Find interface of IsoTree
/// @param [in] boxName Box name that shall be searched after
/// @return Vector of found boxes wrapped with shared_ptr
std::vector<std::shared_ptr<ISO_BASE_BOX>> IsoTree::FindBox(std::string boxName) {

	FindVisitor finder(boxName);

	this->m_root->Accept(finder);

	return finder.GetResults();
}

/// Print interface of IsoTree
void IsoTree::Print() const {

	PrintVisitor v;

	this->m_root->Accept(v);
}

/// Parse interface of IsoTree
/// @param [in] byteBuffer Byte buffer that shall be parsed and from which we will build our IsoTree
void IsoTree::Parse(ByteBuffer& byteBuffer) {

	ParseVisitor parser(this->m_root, byteBuffer);

	while (parser.IsDataAvailable()) {
		parser.AcceptAll();
	}
}

/// ToByteBuffer interface of IsoTree
/// @param [in,out] byteBuffer ByteBuffer that shall be filled from IsoTree
void IsoTree::ToByteBuffer(ByteBuffer& byteBuffer) const {

	OutputCreatorVisitor outputCreator(byteBuffer);

	auto children = this->m_root->GetChildren();
	for (size_t i = 0; i < children.size(); i++) {
		children[i]->Accept(outputCreator);
	}
}

