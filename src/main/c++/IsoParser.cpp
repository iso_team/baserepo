/*
 * IsoParser.cpp
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#include <fstream>
#include "IsoParser.h"

/// Parse method.
/// @param[in] filePath Input File Path
/// @return Shared ptr to the IsoTree
std::shared_ptr<IsoTree> IsoParser::Parse(const char* filePath)
{
	auto isoTree = std::make_shared<IsoTree>();

	std::ifstream ifs(filePath, std::ios::binary | std::ios::in);
	if (ifs.is_open())
	{
		ifs.seekg(0, std::ios::end);
		size_t bufferSize = ifs.tellg();
		ifs.seekg(0, std::ios::beg);

		ByteBuffer byteBuffer(static_cast<unsigned long>(bufferSize + 1));

		byteBuffer.Append(ifs, bufferSize);

		isoTree->Parse(byteBuffer);

		ifs.close();
	}

	return isoTree;
}

