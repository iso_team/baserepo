/*
 * OutputCreatorVisitor.cpp
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */
#include "Visitors/OutputCreatorVisitor.h"
#include "Boxes/IsoBoxes.h"

/////////////////////////////////
/// OFFSET CHECK VISITOR IMPL ///
/////////////////////////////////
void OutputCreatorVisitor::visit(ISO_BASE_BOX* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ICOMPOSITE_BOX* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_COMPOSITE_BOX* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_COMPOSITE_FULLBOX* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_UNKNOWN* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_FTYP* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_MVHD* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_MOOV* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_TRAK* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_TKHD* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_MDIA* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_MDHD* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_HDLR* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_MINF* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_VMHD* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_DINF* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_DREF* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_URL* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_URN* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_STBL* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_STSD* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_AVC1* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_AVCC* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_BASE_BOX_BTRT* box) {
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_STTS* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_STSS* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_STSC* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_STSZ* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}

void OutputCreatorVisitor::visit(ISO_FULLBOX_STCO* box)	{
	box->ToByteBuffer(this->m_refByteBuffer);
}



