/*
 * IsoBoxes.cpp
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

//#include "ParsingContext.h"
#include <cstring>
#include "Boxes/IsoBoxes.h"
#include "ByteBuffer.h"
#include "Util.h"

////////////////////
///// BASE BOX /////
////////////////////
void ISO_BASE_BOX::ToByteBuffer(ByteBuffer& sOut) const {

	if (m_boxSize > 0) {
		sOut.Append(&m_boxSize, sizeof(m_boxSize), true);

		//type 4 bytes of string
		sOut.Append(m_boxName.c_str(), sizeof(uint32_t));
	}
}

void ISO_BASE_BOX::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

void ISO_BASE_BOX::Print() const {
	for (size_t i = 0; i < this->m_boxLevel; i++) {
		std::cout << '|';
	}
	std::cout << "=> Name:" << this->m_boxName << " Size: " << this->m_boxSize << " Offset: " << this->m_boxOffset << std::endl;
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX::GetSelfSptr() const {
	return m_parent->GetChildSptr(this);
}

////////////////////
///// FULL BOX /////
////////////////////
void ISO_FULLBOX::ToByteBuffer(ByteBuffer& sOut) const {

	ISO_BASE_BOX::ToByteBuffer(sOut);
	sOut.Append(&m_versionAndFlags, sizeof(m_versionAndFlags), true);
}

void ISO_FULLBOX::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

/////////////////////////////////
//// COMPOSITE BOX INTERFACE ////
/////////////////////////////////

void ICOMPOSITE_BOX::ToByteBuffer(ByteBuffer& sOut) const
{
	uint32_t childrenCount = static_cast<uint32_t>(m_children.size());
	for (size_t i = 0; i < childrenCount; i++) {
		m_children[i]->ToByteBuffer(sOut);
	}
}

void ICOMPOSITE_BOX::AddChild(std::shared_ptr<ISO_BASE_BOX> sptrChild) {
	this->m_children.push_back(sptrChild);
}

std::shared_ptr<ISO_BASE_BOX> ICOMPOSITE_BOX::GetChildSptr(const ISO_BASE_BOX* const childPtr) {
	size_t childrenCount = this->m_children.size();
	for (size_t i = 0; i < childrenCount; i++) {
		if (this->m_children[i].get() == childPtr) {
			return this->m_children[i];
		}
	}
	return nullptr;
}

std::tuple<int64_t, int64_t, ISO_BASE_BOX*> ICOMPOSITE_BOX::SwapChild(ISO_BASE_BOX* child, std::shared_ptr<ISO_BASE_BOX> newChild) {
	size_t childrenCount = this->m_children.size();
	for (size_t i = 0; i < childrenCount; i++) {
		if (this->m_children[i].get() == child) {
			uint64_t oldStartOffset = this->m_children[i]->GetStartOffset();
			ICOMPOSITE_BOX* oldParent = this->m_children[i]->GetParentBox();

			int64_t oldChildSize = (int64_t)child->GetSize();

			this->m_children[i] = newChild->Clone();

			auto swapInfo = std::make_tuple<int64_t, int64_t, ISO_BASE_BOX*>((int64_t)newChild->GetSize() - oldChildSize,
				(int64_t)newChild->GetStartOffset() - oldStartOffset, this->m_children[i].get());

			this->m_children[i]->SetBoxOffset(oldStartOffset);
			this->m_children[i]->SetParentBox(oldParent);
			return swapInfo;
		}
	}
	return std::make_tuple<int64_t, int64_t, ISO_BASE_BOX*>(0, 0, nullptr);
}

void ICOMPOSITE_BOX::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

////////////////////
// COMPOSITE BOX ///
////////////////////
ISO_BASE_COMPOSITE_BOX::~ISO_BASE_COMPOSITE_BOX() {
}

void ISO_BASE_COMPOSITE_BOX::ToByteBuffer(ByteBuffer& sOut) const
{
	ISO_BASE_BOX::ToByteBuffer(sOut);

	uint32_t childrenCount = static_cast<uint32_t>(m_children.size());
	for (size_t i = 0; i < childrenCount; i++) {
		m_children[i]->ToByteBuffer(sOut);
	}
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_COMPOSITE_BOX::Clone() {
	auto sptrNewBox = std::make_shared<ISO_BASE_COMPOSITE_BOX>(this->m_boxName.c_str(), this->m_parent, this->m_boxSize, this->m_boxOffset, this->m_boxLevel);

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}
	return sptrNewBox;
}

//////////////////////////
/// COMPOSITE FULL BOX ///
//////////////////////////
ISO_COMPOSITE_FULLBOX::~ISO_COMPOSITE_FULLBOX() {
}

void ISO_COMPOSITE_FULLBOX::ToByteBuffer(ByteBuffer& sOut) const {
	this->ISO_BASE_BOX::ToByteBuffer(sOut);

	uint32_t childrenCount = static_cast<uint32_t>(m_children.size());
	for (size_t i = 0; i < childrenCount; i++) {
		m_children[i]->ToByteBuffer(sOut);
	}
}

std::shared_ptr<ISO_BASE_BOX> ISO_COMPOSITE_FULLBOX::Clone() {

	auto sptrNewBox = std::make_shared<ISO_COMPOSITE_FULLBOX>(this->m_boxName.c_str(), this->m_parent, this->m_boxSize, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_versionAndFlags = this->m_versionAndFlags;

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}

	return sptrNewBox;
}

////////////////////
///// UNKN BOX /////
////////////////////
void ISO_BASE_BOX_UNKNOWN::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_BOX::ToByteBuffer(sOut);
	sOut.Append(m_byteBuffer);
}

void ISO_BASE_BOX_UNKNOWN::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_UNKNOWN::Clone() {

	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_UNKNOWN>(this->m_boxName.c_str(), this->m_parent, this->m_boxSize, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_byteBuffer = this->m_byteBuffer;

	return sptrNewBox;
}

////////////////////
///// FTYP BOX /////
////////////////////
void ISO_BASE_BOX_FTYP::ToByteBuffer(ByteBuffer& sOut) const
{
	ISO_BASE_BOX::ToByteBuffer(sOut);

	sOut.Append(&m_majorVersion, sizeof(uint32_t), true);
	sOut.Append(&m_minorVersion, sizeof(uint32_t), true);

	size_t compatibleBrandsSize = m_compatibleBrands.size();
	for (size_t i = 0; i < compatibleBrandsSize; i++) {
		sOut.Append(&m_compatibleBrands[i], sizeof(uint32_t), true);
	}
}

void ISO_BASE_BOX_FTYP::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_FTYP::Clone() {

	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_FTYP>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_majorVersion = this->m_majorVersion;
	sptrNewBox->m_minorVersion = this->m_minorVersion;
	sptrNewBox->m_compatibleBrands = this->m_compatibleBrands;

	return sptrNewBox;
}

//------------------
//---- MVHD BOX ----
//------------------
void ISO_FULLBOX_MVHD::ToByteBuffer(ByteBuffer& sOut) const {

	sOut.Append(&m_rate, sizeof(m_rate), true);
	sOut.Append(&m_volume, sizeof(m_volume), true);
	sOut.Append(&m_reserved, sizeof(m_reserved) / sizeof(m_reserved[0]), false);
	sOut.Append(&m_nextTrackId, sizeof(m_nextTrackId), true);
}

void ISO_FULLBOX_MVHD::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_MVHD::Clone() {
	return nullptr;
}

void ISO_FULLBOX_MVHD_32BIT::ToByteBuffer(ByteBuffer& sOut) const {

	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_creationTime, sizeof(m_creationTime), true);
	sOut.Append(&m_modificationTime, sizeof(m_modificationTime), true);
	sOut.Append(&m_timescale, sizeof(m_timescale), true);
	sOut.Append(&m_duration, sizeof(m_duration), true);

	ISO_FULLBOX_MVHD::ToByteBuffer(sOut);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_MVHD_32BIT::Clone() {
	auto sptrNewBox = std::make_shared<ISO_FULLBOX_MVHD_32BIT>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_rate = this->m_rate;
	sptrNewBox->m_volume = this->m_volume;
	std::memmove(sptrNewBox->m_reserved, this->m_reserved, sizeof(this->m_reserved) / sizeof(this->m_reserved[0]));
	sptrNewBox->m_nextTrackId = this->m_nextTrackId;
	sptrNewBox->m_creationTime = this->m_creationTime;
	sptrNewBox->m_modificationTime = this->m_modificationTime;
	sptrNewBox->m_timescale = this->m_timescale;
	sptrNewBox->m_duration = this->m_duration;

	return sptrNewBox;
}

void ISO_FULLBOX_MVHD_64BIT::ToByteBuffer(ByteBuffer& sOut) const {

	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_creationTime, sizeof(m_creationTime), true);
	sOut.Append(&m_modificationTime, sizeof(m_modificationTime), true);
	sOut.Append(&m_timescale, sizeof(m_timescale), true);
	sOut.Append(&m_duration, sizeof(m_duration), true);

	ISO_FULLBOX_MVHD::ToByteBuffer(sOut);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_MVHD_64BIT::Clone() {
	auto sptrNewBox = std::make_shared<ISO_FULLBOX_MVHD_64BIT>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_rate = this->m_rate;
	sptrNewBox->m_volume = this->m_volume;
	std::memmove(sptrNewBox->m_reserved, this->m_reserved, sizeof(this->m_reserved) / sizeof(this->m_reserved[0]));
	sptrNewBox->m_nextTrackId = this->m_nextTrackId;
	sptrNewBox->m_creationTime = this->m_creationTime;
	sptrNewBox->m_modificationTime = this->m_modificationTime;
	sptrNewBox->m_timescale = this->m_timescale;
	sptrNewBox->m_duration = this->m_duration;

	return sptrNewBox;
}

//------------------
//---- MOOV BOX ----
//------------------
void ISO_BASE_BOX_MOOV::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_COMPOSITE_BOX::ToByteBuffer(sOut);
}

void ISO_BASE_BOX_MOOV::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_MOOV::Clone() {

	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_MOOV>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}

	return sptrNewBox;
}

//------------------
//---- TRAK BOX ----
//------------------
void ISO_BASE_BOX_TRAK::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_COMPOSITE_BOX::ToByteBuffer(sOut);
}

void ISO_BASE_BOX_TRAK::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_TRAK::Clone() {

	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_TRAK>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}

	return sptrNewBox;
}

//------------------
//---- TKHD BOX ----
//------------------
void ISO_FULLBOX_TKHD::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

void ISO_FULLBOX_TKHD::ToByteBuffer(ByteBuffer& sOut) const {

	sOut.Append(&m_reservedStart, sizeof(m_reservedStart) / sizeof(m_reservedStart[0]), false);
	sOut.Append(&m_layer, sizeof(m_layer), true);
	sOut.Append(&m_alternateGroup, sizeof(m_alternateGroup), true);
	sOut.Append(&m_volume, sizeof(m_volume), true);
	sOut.Append(&m_reservedEnd, sizeof(m_reservedEnd) / sizeof(m_reservedEnd[0]), false);
	sOut.Append(&m_width, sizeof(m_width), true);
	sOut.Append(&m_height, sizeof(m_height), true);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_TKHD::Clone() {

	return nullptr;
}

void ISO_FULLBOX_TKHD_32BIT::ToByteBuffer(ByteBuffer& sOut) const {

	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_creationTime, sizeof(m_creationTime), true);
	sOut.Append(&m_modificationTime, sizeof(m_modificationTime), true);
	sOut.Append(&m_trackID, sizeof(m_trackID), true);
	sOut.Append(&m_reserved, sizeof(m_reserved), false);
	sOut.Append(&m_duration, sizeof(m_duration), true);

	ISO_FULLBOX_TKHD::ToByteBuffer(sOut);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_TKHD_32BIT::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_TKHD_32BIT>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	std::memmove(sptrNewBox->m_reservedStart, this->m_reservedStart, sizeof(this->m_reservedStart) / sizeof(this->m_reservedStart[0]));
	sptrNewBox->m_layer = this->m_layer;
	sptrNewBox->m_alternateGroup = this->m_alternateGroup;
	sptrNewBox->m_volume = this->m_volume;
	std::memmove(sptrNewBox->m_reservedEnd, this->m_reservedEnd, sizeof(this->m_reservedEnd) / sizeof(this->m_reservedEnd[0]));
	sptrNewBox->m_width = this->m_width;
	sptrNewBox->m_height = this->m_height;
	sptrNewBox->m_creationTime = this->m_creationTime;
	sptrNewBox->m_modificationTime = this->m_modificationTime;
	sptrNewBox->m_trackID = this->m_trackID;
	sptrNewBox->m_reserved = this->m_reserved;
	sptrNewBox->m_duration = this->m_duration;

	return sptrNewBox;
}

void ISO_FULLBOX_TKHD_64BIT::ToByteBuffer(ByteBuffer& sOut) const {

	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_creationTime, sizeof(m_creationTime), true);
	sOut.Append(&m_modificationTime, sizeof(m_modificationTime), true);
	sOut.Append(&m_trackID, sizeof(m_trackID), true);
	sOut.Append(&m_reserved, sizeof(m_reserved), false);
	sOut.Append(&m_duration, sizeof(m_duration), true);

	ISO_FULLBOX_TKHD::ToByteBuffer(sOut);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_TKHD_64BIT::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_TKHD_64BIT>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	std::memmove(sptrNewBox->m_reservedStart, this->m_reservedStart, sizeof(this->m_reservedStart) / sizeof(this->m_reservedStart[0]));
	sptrNewBox->m_layer = this->m_layer;
	sptrNewBox->m_alternateGroup = this->m_alternateGroup;
	sptrNewBox->m_volume = this->m_volume;
	std::memmove(sptrNewBox->m_reservedEnd, this->m_reservedEnd, sizeof(this->m_reservedEnd) / sizeof(this->m_reservedEnd[0]));
	sptrNewBox->m_width = this->m_width;
	sptrNewBox->m_height = this->m_height;
	sptrNewBox->m_creationTime = this->m_creationTime;
	sptrNewBox->m_modificationTime = this->m_modificationTime;
	sptrNewBox->m_trackID = this->m_trackID;
	sptrNewBox->m_reserved = this->m_reserved;
	sptrNewBox->m_duration = this->m_duration;

	return sptrNewBox;
}

//------------------------
//------- MDIA BOX -------
//------------------------
void ISO_BASE_BOX_MDIA::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_COMPOSITE_BOX::ToByteBuffer(sOut);
}

void ISO_BASE_BOX_MDIA::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_MDIA::Clone() {

	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_MDIA>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}
	return sptrNewBox;
}

//------------------
//---- MDHD BOX ----
//------------------
void ISO_FULLBOX_MDHD::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

void ISO_FULLBOX_MDHD::ToByteBuffer(ByteBuffer& sOut) const {

	sOut.Append(&m_language, sizeof(m_language), true);
	sOut.Append(&m_predefined, sizeof(m_predefined), true);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_MDHD::Clone() {
	return nullptr;
}

void ISO_FULLBOX_MDHD_32BIT::ToByteBuffer(ByteBuffer& sOut) const {

	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_creationTime, sizeof(m_creationTime), true);
	sOut.Append(&m_modificationTime, sizeof(m_modificationTime), true);
	sOut.Append(&m_timescale, sizeof(m_timescale), true);
	sOut.Append(&m_duration, sizeof(m_duration), true);

	ISO_FULLBOX_MDHD::ToByteBuffer(sOut);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_MDHD_32BIT::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_MDHD_32BIT>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_language = this->m_language;
	sptrNewBox->m_predefined = this->m_predefined;
	sptrNewBox->m_creationTime = this->m_creationTime;
	sptrNewBox->m_modificationTime = this->m_modificationTime;
	sptrNewBox->m_timescale = this->m_timescale;
	sptrNewBox->m_duration = this->m_duration;

	return sptrNewBox;
}

void ISO_FULLBOX_MDHD_64BIT::ToByteBuffer(ByteBuffer& sOut) const {

	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_creationTime, sizeof(m_creationTime), true);
	sOut.Append(&m_modificationTime, sizeof(m_modificationTime), true);
	sOut.Append(&m_timescale, sizeof(m_timescale), true);
	sOut.Append(&m_duration, sizeof(m_duration), true);

	ISO_FULLBOX_MDHD::ToByteBuffer(sOut);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_MDHD_64BIT::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_MDHD_64BIT>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_language = this->m_language;
	sptrNewBox->m_predefined = this->m_predefined;
	sptrNewBox->m_creationTime = this->m_creationTime;
	sptrNewBox->m_modificationTime = this->m_modificationTime;
	sptrNewBox->m_timescale = this->m_timescale;
	sptrNewBox->m_duration = this->m_duration;

	return sptrNewBox;
}

//------------------
//---- HDLR BOX ----
//------------------
void ISO_FULLBOX_HDLR::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_predefined, sizeof(m_predefined), true);
	sOut.Append(&m_handlerType, sizeof(m_handlerType), true);
	sOut.Append(&m_reserved, sizeof(m_reserved) / sizeof(m_reserved[0]), false);
	sOut.Append(m_name.c_str(), m_name.size());
}

void ISO_FULLBOX_HDLR::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_HDLR::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_HDLR>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_predefined = this->m_predefined;
	sptrNewBox->m_handlerType = this->m_handlerType;
	std::memmove(sptrNewBox->m_reserved, this->m_reserved, sizeof(this->m_reserved) / sizeof(this->m_reserved[0]));
	sptrNewBox->m_name = this->m_name;

	return sptrNewBox;
}

//------------------------
//------- MINF BOX -------
//------------------------
void ISO_BASE_BOX_MINF::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_COMPOSITE_BOX::ToByteBuffer(sOut);
}

void ISO_BASE_BOX_MINF::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_MINF::Clone() {

	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_MINF>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}
	return sptrNewBox;
}

//------------------------
//------- VMHD BOX -------
//------------------------
void ISO_FULLBOX_VMHD::ToByteBuffer(ByteBuffer& sOut) const{
	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_graphicsMode, sizeof(m_graphicsMode), true);
	sOut.Append(&m_opColor, sizeof(m_opColor), true);
}

void ISO_FULLBOX_VMHD::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_VMHD::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_VMHD>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_graphicsMode = this->m_graphicsMode;
	sptrNewBox->m_opColor = this->m_opColor;

	return sptrNewBox;
}

//------------------------
//------- DINF BOX -------
//------------------------
void ISO_BASE_BOX_DINF::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_COMPOSITE_BOX::ToByteBuffer(sOut);
}

void ISO_BASE_BOX_DINF::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_DINF::Clone() {

	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_DINF>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}
	return sptrNewBox;
}

//------------------------
//------- DREF BOX -------
//------------------------
void ISO_FULLBOX_DREF::ToByteBuffer(ByteBuffer& sOut) const {

	ISO_BASE_BOX::ToByteBuffer(sOut);

	sOut.Append(&m_versionAndFlags, sizeof(m_versionAndFlags), true);
	sOut.Append(&m_entryCount, sizeof(m_entryCount), true);

	ICOMPOSITE_BOX::ToByteBuffer(sOut);
}

void ISO_FULLBOX_DREF::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_DREF::Clone() {
	std::shared_ptr<ISO_FULLBOX_DREF> sptrNewBox = std::make_shared<ISO_FULLBOX_DREF>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_versionAndFlags = this->m_versionAndFlags;

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}

	return sptrNewBox;
}

//------------------------
//------- URL BOX -------
//------------------------
void ISO_FULLBOX_URL::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_FULLBOX::ToByteBuffer(sOut);
	sOut.Append(m_location.c_str(), m_location.size());
}


void ISO_FULLBOX_URL::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_URL::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_URL>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_location = this->m_location;

	return sptrNewBox;
}

//------------------------
//------- URN BOX -------
//------------------------
void ISO_FULLBOX_URN::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_FULLBOX::ToByteBuffer(sOut);
	sOut.Append(m_name.c_str(), m_name.size());
	sOut.Append(m_location.c_str(), m_location.size());
}

void ISO_FULLBOX_URN::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_URN::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_URN>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_name = this->m_name;
	sptrNewBox->m_location = this->m_location;

	return sptrNewBox;
}

//------------------------
//------- STBL BOX -------
//------------------------
void ISO_BASE_BOX_STBL::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_COMPOSITE_BOX::ToByteBuffer(sOut);
}

void ISO_BASE_BOX_STBL::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_STBL::Clone() {
	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_STBL>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}
	return sptrNewBox;
}

//------------------------
//------- STSD BOX -------
//------------------------
void ISO_FULLBOX_STSD::ToByteBuffer(ByteBuffer& sOut) const {

	ISO_BASE_BOX::ToByteBuffer(sOut);

	sOut.Append(&m_versionAndFlags, sizeof(m_versionAndFlags), true);
	sOut.Append(&m_numEntries, sizeof(m_numEntries), true);

	ICOMPOSITE_BOX::ToByteBuffer(sOut);
}

void ISO_FULLBOX_STSD::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_STSD::Clone() {

	std::shared_ptr<ISO_FULLBOX_STSD> sptrNewBox = std::make_shared<ISO_FULLBOX_STSD>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_versionAndFlags = this->m_versionAndFlags;
	sptrNewBox->m_numEntries = this->m_numEntries;

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}

	return sptrNewBox;
}

//------------------------
//------- AVC1 BOX -------
//------------------------
void ISO_FULLBOX_AVC1::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_BOX::ToByteBuffer(sOut);

	sOut.Append(&m_versionAndFlags, sizeof(m_versionAndFlags), true);
	sOut.Append(&m_reservedStart, sizeof(m_reservedStart) / sizeof(m_reservedStart[0]), true);
	sOut.Append(&m_dataRefIndex, sizeof(m_dataRefIndex), true);
	sOut.Append(&m_reservedEnd, sizeof(m_reservedEnd) / sizeof(m_reservedEnd[0]), true);
	sOut.Append(&m_width, sizeof(m_width), true);
	sOut.Append(&m_height, sizeof(m_height), true);
	sOut.Append(&m_horRes, sizeof(m_horRes), true);
	sOut.Append(&m_unknwnHorValue, sizeof(m_unknwnHorValue), true);
	sOut.Append(&m_vertRes, sizeof(m_vertRes), true);
	sOut.Append(&m_compressorName, sizeof(m_compressorName) / sizeof(m_compressorName[0]), false);
	sOut.Append(&m_colorDepth, sizeof(m_colorDepth), true);
	sOut.Append(&m_predefined, sizeof(m_predefined), true);

	ICOMPOSITE_BOX::ToByteBuffer(sOut);
}

void ISO_FULLBOX_AVC1::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_AVC1::Clone() {

	std::shared_ptr<ISO_FULLBOX_AVC1> sptrNewBox = std::make_shared<ISO_FULLBOX_AVC1>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_versionAndFlags = this->m_versionAndFlags;
	sptrNewBox->m_reservedStart = this->m_reservedStart;
	sptrNewBox->m_dataRefIndex = this->m_dataRefIndex;
	sptrNewBox->m_reservedEnd = this->m_reservedEnd;
	sptrNewBox->m_width = this->m_width;
	sptrNewBox->m_height = this->m_height;
	sptrNewBox->m_horRes = this->m_horRes;
	sptrNewBox->m_unknwnHorValue = this->m_unknwnHorValue;
	sptrNewBox->m_vertRes = this->m_vertRes;
	sptrNewBox->m_compressorName = this->m_compressorName;
	sptrNewBox->m_colorDepth = this->m_colorDepth;
	sptrNewBox->m_predefined = this->m_predefined;

	for (size_t i = 0; i < this->m_children.size(); i++){
		sptrNewBox->m_children.push_back(this->m_children[i]->Clone());
	}

	return sptrNewBox;
}

//------------------------
//------- AVCC BOX -------
//------------------------
ISO_BASE_BOX_AVCC::~ISO_BASE_BOX_AVCC() {
	size_t i = 0;
	for (; i < m_spsCount; i++) {
		delete m_spsEntries[i];
		m_spsEntries[i] = nullptr;
	}
	for (i = 0; i < m_ppsCount; i++) {
		delete m_ppsEntries[i];
		m_ppsEntries[i] = nullptr;
	}
}

void ISO_BASE_BOX_AVCC::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_BOX::ToByteBuffer(sOut);

	sOut.Append(&m_configVersion, sizeof(m_configVersion), true);
	sOut.Append(&m_avcProfileIndication, sizeof(m_avcProfileIndication), true);
	sOut.Append(&m_profileCompatibility, sizeof(m_profileCompatibility), true);
	sOut.Append(&m_avcLevelIndication, sizeof(m_avcLevelIndication), true);

	uint8_t fileLength = (m_length - 1) | 0xFC;
	sOut.Append(&fileLength, sizeof(fileLength), true);
	uint8_t fileSpsCount = m_spsCount | 0xE0;
	sOut.Append(&fileSpsCount, sizeof(fileSpsCount), true);

	for (auto i = 0; i < m_spsCount; i++) {
		uint16_t spsSize = static_cast<uint16_t>(m_spsEntries[i]->GetSize());
		sOut.Append(&spsSize, sizeof(spsSize), true);
		sOut.Append((const char*)m_spsEntries[i]->DataPtr(), m_spsEntries[i]->GetSize(), false);
	}

	sOut.Append(&m_ppsCount, sizeof(m_ppsCount), true);
	for (auto i = 0; i < m_ppsCount; i++) {
		uint16_t ppsSize = static_cast<uint16_t>(m_ppsEntries[i]->GetSize());
		sOut.Append(&ppsSize, sizeof(ppsSize), true);
		sOut.Append((const char*)m_ppsEntries[i]->DataPtr(), m_ppsEntries[i]->GetSize(), false);
	}

}

void ISO_BASE_BOX_AVCC::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_AVCC::Clone() {

	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_AVCC>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_configVersion = this->m_configVersion;
	sptrNewBox->m_avcProfileIndication = this->m_avcProfileIndication;
	sptrNewBox->m_profileCompatibility = this->m_profileCompatibility;
	sptrNewBox->m_avcLevelIndication = this->m_avcLevelIndication;
	sptrNewBox->m_length = this->m_length;

	sptrNewBox->m_spsCount = this->m_spsCount;
	for (size_t i = 0; i < this->m_spsCount; i++) {
		ByteBuffer* newByteBuffer = new ByteBuffer(*this->m_spsEntries[i]);
		sptrNewBox->m_spsEntries.push_back(newByteBuffer);
	}

	sptrNewBox->m_ppsCount = this->m_ppsCount;
	for (size_t i = 0; i < this->m_spsCount; i++) {
		ByteBuffer* newByteBuffer = new ByteBuffer(*this->m_ppsEntries[i]);
		sptrNewBox->m_ppsEntries.push_back(newByteBuffer);
	}

	return sptrNewBox;
}

uint32_t ISO_BASE_BOX_AVCC::GetHeaderSize() const {
	uint32_t headerSize = this->ISO_BASE_BOX::GetHeaderSize() +
		sizeof(m_configVersion) + sizeof(m_avcProfileIndication) +
		sizeof(m_profileCompatibility) + sizeof(m_avcLevelIndication) +
		sizeof(m_length) + sizeof(m_spsCount) + sizeof(m_ppsCount);

	for (auto i = 0; i < m_spsCount; i++) {
		headerSize += m_spsEntries[i]->GetSize();
	}
	for (auto i = 0; i < m_ppsCount; i++) {
		headerSize += m_ppsEntries[i]->GetSize();
	}
	return headerSize;
}

//------------------------
//------- BTRT BOX -------
//------------------------
void ISO_BASE_BOX_BTRT::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_BASE_BOX::ToByteBuffer(sOut);

	sOut.Append(&m_bufferSize, sizeof(m_bufferSize), true);
	sOut.Append(&m_maxBitrate, sizeof(m_maxBitrate), true);
	sOut.Append(&m_minBitrate, sizeof(m_minBitrate), true);
}

void ISO_BASE_BOX_BTRT::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_BASE_BOX_BTRT::Clone() {

	auto sptrNewBox = std::make_shared<ISO_BASE_BOX_BTRT>(this->m_boxSize, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_bufferSize = this->m_bufferSize;
	sptrNewBox->m_maxBitrate = this->m_maxBitrate;
	sptrNewBox->m_minBitrate = this->m_minBitrate;

	return sptrNewBox;
}

//------------------------
//------- STTS BOX -------
//------------------------
void ISO_FULLBOX_STTS::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_entryCount, sizeof(m_entryCount), true);

	for (uint32_t i = 0; i < m_entryCount; i++) {
		sOut.Append(&m_sttsEntries[i].m_sampleCount, sizeof(m_sttsEntries[i].m_sampleCount), true);
		sOut.Append(&m_sttsEntries[i].m_sampleDelta, sizeof(m_sttsEntries[i].m_sampleDelta), true);
	}
}

void ISO_FULLBOX_STTS::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_STTS::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_STTS>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_entryCount = this->m_entryCount;
	sptrNewBox->m_sttsEntries = this->m_sttsEntries;

	return sptrNewBox;
}

//------------------------
//------- STSS BOX -------
//------------------------
void ISO_FULLBOX_STSS::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_entryCount, sizeof(m_entryCount), true);

	for (uint32_t i = 0; i < m_entryCount; i++) {
		sOut.Append(&m_stssEntries[i], sizeof(m_stssEntries[i]), true);
	}
}

void ISO_FULLBOX_STSS::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_STSS::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_STSS>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_entryCount = this->m_entryCount;
	sptrNewBox->m_stssEntries = this->m_stssEntries;

	return sptrNewBox;
}

//------------------------
//------- STSC BOX -------
//------------------------
void ISO_FULLBOX_STSC::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_entryCount, sizeof(m_entryCount), true);

	for (uint32_t i = 0; i < m_entryCount; i++) {
		sOut.Append(&m_stscEntries[i].m_firstChunk, sizeof(m_stscEntries[i].m_firstChunk), true);
		sOut.Append(&m_stscEntries[i].m_samplesPerChunk, sizeof(m_stscEntries[i].m_samplesPerChunk), true);
		sOut.Append(&m_stscEntries[i].m_sampleDescIndex, sizeof(m_stscEntries[i].m_sampleDescIndex), true);
	}
}

void ISO_FULLBOX_STSC::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_STSC::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_STSC>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_entryCount = this->m_entryCount;
	sptrNewBox->m_stscEntries = this->m_stscEntries;

	return sptrNewBox;
}

//------------------------
//------- STSZ BOX -------
//------------------------
void ISO_FULLBOX_STSZ::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_sampleSize, sizeof(m_sampleSize), true);
	sOut.Append(&m_sampleCount, sizeof(m_sampleCount), true);

	for (uint32_t i = 0; i < m_sampleCount; i++) {
		sOut.Append(&m_entrySizes[i], sizeof(m_entrySizes[i]), true);
	}
}

void ISO_FULLBOX_STSZ::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_STSZ::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_STSZ>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_sampleSize = this->m_sampleSize;
	sptrNewBox->m_sampleCount = this->m_sampleCount;
	sptrNewBox->m_entrySizes = this->m_entrySizes;

	return sptrNewBox;
}

//------------------------
//------- STCO BOX -------
//------------------------
void ISO_FULLBOX_STCO::ToByteBuffer(ByteBuffer& sOut) const {
	ISO_FULLBOX::ToByteBuffer(sOut);

	sOut.Append(&m_entryCount, sizeof(m_entryCount), true);

	for (uint32_t i = 0; i < m_entryCount; i++) {
		sOut.Append(&m_chunkOffsets[i], sizeof(m_chunkOffsets[i]), true);
	}
}

void ISO_FULLBOX_STCO::Accept(IVisitor& refVisitor) {
	refVisitor.visit(this);
}

std::shared_ptr<ISO_BASE_BOX> ISO_FULLBOX_STCO::Clone() {

	auto sptrNewBox = std::make_shared<ISO_FULLBOX_STCO>(this->m_boxSize, this->m_versionAndFlags, this->m_parent, this->m_boxOffset, this->m_boxLevel);

	sptrNewBox->m_entryCount = this->m_entryCount;
	sptrNewBox->m_chunkOffsets = this->m_chunkOffsets;

	return sptrNewBox;
}

/// Used for offset recalculation if the boxes are moved/changed/deleted/shrinked
void ISO_FULLBOX_STCO::UpdateOffsets(uint32_t mdatOffset) {

	int32_t delta = mdatOffset;
	delta -= this->m_chunkOffsets[0];

	int32_t MAGIC_NUM_SPS_AND_PPS_STARTING_THE_MDAT = 0;

	for (size_t i = 0; i < this->m_entryCount; i++) {
		this->m_chunkOffsets[i] += delta + MAGIC_NUM_SPS_AND_PPS_STARTING_THE_MDAT;
	}
}


