/*
 * FindVisitor.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_VISITORS_FINDVISITOR_H_
#define SRC_MAIN_INCLUDE_VISITORS_FINDVISITOR_H_

#include <memory>
#include <vector>
#include "Visitors/IVisitor.h"


////////////////////////////////////////////////////////////////////////
/// Visitor for IsoTree box lookup
/// Within one file we can have multiple boxes with the same name
/// so the end result of the search is the vector of box shared pointers
////////////////////////////////////////////////////////////////////////
class FindVisitor : public IVisitor
{
private:
	std::vector<std::string>					m_boxHierarchyToFind;			///< Box hierarchy that shall be looked for
	std::vector<std::shared_ptr<ISO_BASE_BOX>>	m_findResults;					///< Lookup results
	size_t										m_boxHierarchyIndexToHit;		///< Used for hierarchy lookup
public:
	/// ctor
	FindVisitor(std::string boxName)
		: m_boxHierarchyToFind(1, boxName),
		  m_boxHierarchyIndexToHit(0) {
	}

	/// @param boxHierarchyToFind
	FindVisitor(std::vector<std::string> boxHierarchyToFind)
		: m_boxHierarchyToFind(boxHierarchyToFind),
		  m_boxHierarchyIndexToHit(0) {
	}

	/// dtor
	~FindVisitor() {
	}

	/// Fetch results
	/// @return m_findResults
	std::vector<std::shared_ptr<ISO_BASE_BOX>> GetResults() {
		return this->m_findResults;
	}

	void visit(ISO_BASE_BOX* box) override;
	void visit(ISO_FULLBOX* box) override;
	void visit(ICOMPOSITE_BOX* box) override;
	void visit(ISO_BASE_COMPOSITE_BOX* box) override;
	void visit(ISO_COMPOSITE_FULLBOX* box) override;
	void visit(ISO_BASE_BOX_UNKNOWN* box) override;
	void visit(ISO_BASE_BOX_FTYP* box) override;
	void visit(ISO_FULLBOX_MVHD* box) override;
	void visit(ISO_BASE_BOX_MOOV* box) override;
	void visit(ISO_BASE_BOX_TRAK* box) override;
	void visit(ISO_FULLBOX_TKHD* box) override;
	void visit(ISO_BASE_BOX_MDIA* box) override;
	void visit(ISO_FULLBOX_MDHD* box) override;
	void visit(ISO_FULLBOX_HDLR* box) override;
	void visit(ISO_BASE_BOX_MINF* box) override;
	void visit(ISO_FULLBOX_VMHD* box) override;
	void visit(ISO_BASE_BOX_DINF* box) override;
	void visit(ISO_FULLBOX_DREF* box) override;
	void visit(ISO_FULLBOX_URL* box) override;
	void visit(ISO_FULLBOX_URN* box) override;
	void visit(ISO_BASE_BOX_STBL* box) override;
	void visit(ISO_FULLBOX_STSD* box) override;
	void visit(ISO_FULLBOX_AVC1* box) override;
	void visit(ISO_BASE_BOX_AVCC* box) override;
	void visit(ISO_BASE_BOX_BTRT* box) override;
	void visit(ISO_FULLBOX_STTS* box) override;
	void visit(ISO_FULLBOX_STSS* box) override;
	void visit(ISO_FULLBOX_STSC* box) override;
	void visit(ISO_FULLBOX_STSZ* box) override;
	void visit(ISO_FULLBOX_STCO* box) override;

private:
	void FindBox(const ISO_BASE_BOX* const box);
};

#endif /* SRC_MAIN_INCLUDE_VISITORS_FINDVISITOR_H_ */
