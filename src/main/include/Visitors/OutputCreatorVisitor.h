/*
 * OutputCreatorVisitor.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_VISITORS_OUTPUTCREATORVISITOR_H_
#define SRC_MAIN_INCLUDE_VISITORS_OUTPUTCREATORVISITOR_H_

#include "Visitors/IVisitor.h"
#include "ByteBuffer.h"

///////////////////////////////////////////////////////////
/// Transformation from a IsoTree to a ByteBuffer made easy
/// with this visitor'ish class
///////////////////////////////////////////////////////////
class OutputCreatorVisitor : public IVisitor
{
private:
	ByteBuffer& m_refByteBuffer;

public:
	/// Constructor with the reference to an output buffer
	OutputCreatorVisitor(ByteBuffer& refByteBuffer) : m_refByteBuffer(refByteBuffer) {
	}

	void visit(ISO_BASE_BOX* box) override;
	void visit(ISO_FULLBOX* box) override;
	void visit(ICOMPOSITE_BOX* box) override;
	void visit(ISO_BASE_COMPOSITE_BOX* box) override;
	void visit(ISO_COMPOSITE_FULLBOX* box) override;
	void visit(ISO_BASE_BOX_UNKNOWN* box) override;
	void visit(ISO_BASE_BOX_FTYP* box) override;
	void visit(ISO_FULLBOX_MVHD* box) override;
	void visit(ISO_BASE_BOX_MOOV* box) override;
	void visit(ISO_BASE_BOX_TRAK* box) override;
	void visit(ISO_FULLBOX_TKHD* box) override;
	void visit(ISO_BASE_BOX_MDIA* box) override;
	void visit(ISO_FULLBOX_MDHD* box) override;
	void visit(ISO_FULLBOX_HDLR* box) override;
	void visit(ISO_BASE_BOX_MINF* box) override;
	void visit(ISO_FULLBOX_VMHD* box) override;
	void visit(ISO_BASE_BOX_DINF* box) override;
	void visit(ISO_FULLBOX_DREF* box) override;
	void visit(ISO_FULLBOX_URL* box) override;
	void visit(ISO_FULLBOX_URN* box) override;
	void visit(ISO_BASE_BOX_STBL* box) override;
	void visit(ISO_FULLBOX_STSD* box) override;
	void visit(ISO_FULLBOX_AVC1* box) override;
	void visit(ISO_BASE_BOX_AVCC* box) override;
	void visit(ISO_BASE_BOX_BTRT* box) override;
	void visit(ISO_FULLBOX_STTS* box) override;
	void visit(ISO_FULLBOX_STSS* box) override;
	void visit(ISO_FULLBOX_STSC* box) override;
	void visit(ISO_FULLBOX_STSZ* box) override;
	void visit(ISO_FULLBOX_STCO* box) override;
};

#endif /* SRC_MAIN_INCLUDE_VISITORS_OUTPUTCREATORVISITOR_H_ */
