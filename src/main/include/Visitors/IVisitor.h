/*
 * IVisitor.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_VISITORS_IVISITOR_H_
#define SRC_MAIN_INCLUDE_VISITORS_IVISITOR_H_

struct ISO_BASE_BOX;
struct ISO_FULLBOX;
struct ICOMPOSITE_BOX;
struct ISO_BASE_COMPOSITE_BOX;
struct ISO_COMPOSITE_FULLBOX;
struct ISO_BASE_BOX_UNKNOWN;
struct ISO_BASE_BOX_FTYP;
struct ISO_FULLBOX_MVHD;
struct ISO_BASE_BOX_MOOV;
struct ISO_BASE_BOX_TRAK;
struct ISO_FULLBOX_TKHD;
struct ISO_BASE_BOX_MDIA;
struct ISO_FULLBOX_MDHD;
struct ISO_FULLBOX_HDLR;
struct ISO_BASE_BOX_MINF;
struct ISO_FULLBOX_VMHD;
struct ISO_BASE_BOX_DINF;
struct ISO_FULLBOX_DREF;
struct ISO_FULLBOX_URL;
struct ISO_FULLBOX_URN;
struct ISO_BASE_BOX_STBL;
struct ISO_FULLBOX_STSD;
struct ISO_FULLBOX_AVC1;
struct ISO_BASE_BOX_AVCC;
struct ISO_BASE_BOX_BTRT;
struct ISO_FULLBOX_STTS;
struct ISO_FULLBOX_STSS;
struct ISO_FULLBOX_STSC;
struct ISO_FULLBOX_STSZ;
struct ISO_FULLBOX_STCO;

/// Pure abstract class for Visitor-like objects
class IVisitor
{
public:
	virtual void visit(ISO_BASE_BOX* box) = 0;				///< ISO_BASE_BOX visit interface
	virtual void visit(ISO_FULLBOX* box) = 0;				///< ISO_FULLBOX visit interface
	virtual void visit(ICOMPOSITE_BOX* box) = 0;			///< ICOMPOSITE_BOX visit interface
	virtual void visit(ISO_BASE_COMPOSITE_BOX* box) = 0;	///< ISO_BASE_COMPOSITE_BOX visit interface
	virtual void visit(ISO_COMPOSITE_FULLBOX* box) = 0;		///< ISO_COMPOSITE_FULLBOX visit interface
	virtual void visit(ISO_BASE_BOX_UNKNOWN* box) = 0;		///< ISO_BASE_BOX_UNKNOWN visit interface
	virtual void visit(ISO_BASE_BOX_FTYP* box) = 0;			///< ISO_BASE_BOX_FTYP visit interface
	virtual void visit(ISO_FULLBOX_MVHD* box) = 0;			///< ISO_FULLBOX_MVHD visit interface
	virtual void visit(ISO_BASE_BOX_MOOV* box) = 0;			///< ISO_BASE_BOX_MOOV visit interface
	virtual void visit(ISO_BASE_BOX_TRAK* box) = 0;			///< ISO_BASE_BOX_TRAK visit interface
	virtual void visit(ISO_FULLBOX_TKHD* box) = 0;			///< ISO_FULLBOX_TKHD visit interface
	virtual void visit(ISO_BASE_BOX_MDIA* box) = 0;			///< ISO_BASE_BOX_MDIA visit interface
	virtual void visit(ISO_FULLBOX_MDHD* box) = 0;			///< ISO_FULLBOX_MDHD visit interface
	virtual void visit(ISO_FULLBOX_HDLR* box) = 0;			///< ISO_FULLBOX_HDLR visit interface
	virtual void visit(ISO_BASE_BOX_MINF* box) = 0;			///< ISO_BASE_BOX_MINF visit interface
	virtual void visit(ISO_FULLBOX_VMHD* box) = 0;			///< ISO_FULLBOX_VMHD visit interface
	virtual void visit(ISO_BASE_BOX_DINF* box) = 0;			///< ISO_BASE_BOX_DINF visit interface
	virtual void visit(ISO_FULLBOX_DREF* box) = 0;			///< ISO_FULLBOX_DREF visit interface
	virtual void visit(ISO_FULLBOX_URL* box) = 0;			///< ISO_FULLBOX_URL visit interface
	virtual void visit(ISO_FULLBOX_URN* box) = 0;			///< ISO_FULLBOX_URN visit interface
	virtual void visit(ISO_BASE_BOX_STBL* box) = 0;			///< ISO_BASE_BOX_STBL visit interface
	virtual void visit(ISO_FULLBOX_STSD* box) = 0;			///< ISO_FULLBOX_STSD visit interface
	virtual void visit(ISO_FULLBOX_AVC1* box) = 0;			///< ISO_FULLBOX_AVC1 visit interface
	virtual void visit(ISO_BASE_BOX_AVCC* box) = 0;			///< ISO_BASE_BOX_AVCC visit interface
	virtual void visit(ISO_BASE_BOX_BTRT* box) = 0;			///< ISO_BASE_BOX_BTRT visit interface
	virtual void visit(ISO_FULLBOX_STTS* box) = 0;			///< ISO_FULLBOX_STTS visit interface
	virtual void visit(ISO_FULLBOX_STSS* box) = 0;			///< ISO_FULLBOX_STSS visit interface
	virtual void visit(ISO_FULLBOX_STSC* box) = 0;			///< ISO_FULLBOX_STSC visit interface
	virtual void visit(ISO_FULLBOX_STSZ* box) = 0;			///< ISO_FULLBOX_STSZ visit interface
	virtual void visit(ISO_FULLBOX_STCO* box) = 0;			///< ISO_FULLBOX_STCO visit interface
	virtual ~IVisitor() {
	};
};

#endif /* SRC_MAIN_INCLUDE_VISITORS_IVISITOR_H_ */
