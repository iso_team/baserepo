/*
 * ParseVisitor.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_VISITORS_PARSEVISITOR_H_
#define SRC_MAIN_INCLUDE_VISITORS_PARSEVISITOR_H_

#include <memory>
#include <stack>
#include <vector>
#include <cstdint>
#include "Visitors/IVisitor.h"
#include "Util.h"
#include "ByteBuffer.h"

/// Parse visitor used to transform inputByteBuffer to a
/// tree-like structure that ISO base media format follows.
class ParseVisitor: public IVisitor
{
private:
	ByteBuffer& m_refData;													///< Input data reference
	uint64_t m_dataOffset;													///< Offset of a parsing pivot
	std::stack<std::shared_ptr<ICOMPOSITE_BOX>> m_compositeBoxes;			///< Tracks parent boxes during parsing procedure
	std::vector<ISO_BASE_BOX*> m_isoBoxesParsingFragments;					///< List of well defined boxes (visitor ability)
	uint64_t m_currentBox_Offset;											///< Offset of currently parsed box (relative to the start of a file)
	uint64_t m_currentBox_Size;												///< Size of currently parsed box (relative to the start of a file)

public:
	ParseVisitor(std::shared_ptr<ISO_BASE_COMPOSITE_BOX> rootBox, ByteBuffer& byteBuffer, uint64_t dataOffset = 0);

	~ParseVisitor();

	void AcceptAll();

	bool IsDataAvailable();

private:
	void Parse(std::shared_ptr<ISO_BASE_COMPOSITE_BOX> newComposite);

	void Parse(std::shared_ptr<ISO_COMPOSITE_FULLBOX> newComposite);

	void PushParentBox(std::shared_ptr<ICOMPOSITE_BOX> sptrParentBox);

	void PopParentBox();

	std::string GetNextBoxNameParentBox();

	bool AreOffsetsInValidRange(uint32_t sizeOfTheDataToRead);

	/// Utility method for integer parsing (uint8_t, unt16_t, uint32_t, uint64_t) from a byte buffer
	template<typename T>
	void ReadInteger(T& value) {

		if (this->AreOffsetsInValidRange(sizeof(T))) {
			Util::ReadBytesToInt(this->m_refData.DataPtr() + this->m_dataOffset, value);
			this->m_dataOffset += sizeof(T);
		}
	}

	/// Utility method for integer array parsing (uint8_t, unt16_t, uint32_t, uint64_t) from a byte buffer
	template<typename T>
	void ReadIntegerArray(T& integerArray) {

		size_t arraySize = sizeof(integerArray) / sizeof(integerArray[0]);
		for (size_t i = 0; i < arraySize; i++) {
			this->ReadInteger(integerArray[i]);
		}
	}

	/// Utility method for string parsing from a byte buffer
	/// String must give std::assign like interface to be fed into this method
	template<typename T>
	void ReadString(T& value, size_t stringLength) {

		if (this->AreOffsetsInValidRange(static_cast<uint32_t>(stringLength))) {
			value.assign(reinterpret_cast<const char*>(this->m_refData.DataPtr() + this->m_dataOffset), stringLength);
			this->m_dataOffset += stringLength;
		}
	}

	/// Utility method for string parsing from a byte buffer. Byte buffer holds terminating character.
	/// String must give std::assign like interface to be fed into this method
	template<typename T>
	void ReadTerminatedString(T& value) {

		uint32_t stringLength = 0;
		while ((*(this->m_refData.DataPtr() + this->m_dataOffset + stringLength) != '\0') && this->AreOffsetsInValidRange(stringLength)) {
			stringLength += 1;
		}

		// Note: String length + 1 just to include '\0' as he is also part of the data block
		value.assign(reinterpret_cast<const char*>(this->m_refData.DataPtr() + this->m_dataOffset), stringLength + 1);
		this->m_dataOffset += stringLength + 1;
	}

public:

	void visit(ISO_BASE_BOX* box) override;
	void visit(ISO_FULLBOX* box) override;
	void visit(ICOMPOSITE_BOX* box) override;
	void visit(ISO_BASE_COMPOSITE_BOX* box) override;
	void visit(ISO_COMPOSITE_FULLBOX* box) override;
	void visit(ISO_BASE_BOX_UNKNOWN* box) override;
	void visit(ISO_BASE_BOX_FTYP* box) override;
	void visit(ISO_FULLBOX_MVHD* box) override;
	void visit(ISO_BASE_BOX_MOOV* box) override;
	void visit(ISO_BASE_BOX_TRAK* box) override;
	void visit(ISO_FULLBOX_TKHD* box) override;
	void visit(ISO_BASE_BOX_MDIA* box) override;
	void visit(ISO_FULLBOX_MDHD* box) override;
	void visit(ISO_FULLBOX_HDLR* box) override;
	void visit(ISO_BASE_BOX_MINF* box) override;
	void visit(ISO_FULLBOX_VMHD* box) override;
	void visit(ISO_BASE_BOX_DINF* box) override;
	void visit(ISO_FULLBOX_DREF* box) override;
	void visit(ISO_FULLBOX_URL* box) override;
	void visit(ISO_FULLBOX_URN* box) override;
	void visit(ISO_BASE_BOX_STBL* box) override;
	void visit(ISO_FULLBOX_STSD* box) override;
	void visit(ISO_FULLBOX_AVC1* box) override;
	void visit(ISO_BASE_BOX_AVCC* box) override;
	void visit(ISO_BASE_BOX_BTRT* box) override;
	void visit(ISO_FULLBOX_STTS* box) override;
	void visit(ISO_FULLBOX_STSS* box) override;
	void visit(ISO_FULLBOX_STSC* box) override;
	void visit(ISO_FULLBOX_STSZ* box) override;
	void visit(ISO_FULLBOX_STCO* box) override;
};

#endif /* SRC_MAIN_INCLUDE_VISITORS_PARSEVISITOR_H_ */
