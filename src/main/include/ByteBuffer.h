/*
 * ByteBuffer.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BYTEBUFFER_H_
#define SRC_MAIN_INCLUDE_BYTEBUFFER_H_

#include <cstdlib>
#include <stddef.h>
#include <fstream>

/// ////////////////////////////////////
/// Class represents custom byte buffer
/// This collection holds unsigned chars
/// and is of "unlimited" length
/// ////////////////////////////////////
class ByteBuffer
{
private:
	unsigned char*	m_ptrBuffer;
	unsigned long 	m_capacity;
	unsigned long 	m_usedCapacity;
public:

	ByteBuffer();

	ByteBuffer(unsigned long bufferSize);

	ByteBuffer(const ByteBuffer& rhs);

	~ByteBuffer();

	ByteBuffer& operator=(const ByteBuffer& byteBuffer);

	bool operator==(const ByteBuffer& byteBuffer);

	void assign(const void* ptrBuffer, size_t bufferSize, bool doSwap = false);

	void Append(const void* ptrBuffer, size_t bufferSize, bool doSwap = false);

	void Append(const ByteBuffer& byteBuffer, bool doSwap = false);

	void Append(std::ifstream& ifs, size_t count);

	void Reserve(size_t size);

	unsigned char* DataPtr() const;

	unsigned long GetSize() const;

	void Empty();

	char* c_str();

	void SetLength(size_t usedCapacity);

	void Alloc(size_t capacity);
};

#endif /* SRC_MAIN_INCLUDE_BYTEBUFFER_H_ */
