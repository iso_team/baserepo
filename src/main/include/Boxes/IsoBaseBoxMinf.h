/*
 * IsoBaseBoxMinf.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXMINF_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXMINF_H_


/// Media Information Box
/// For more details consult ISO/IEC::14496-12
struct ISO_BASE_BOX_MINF : public ISO_BASE_COMPOSITE_BOX
{
public:
	/// ctor
	ISO_BASE_BOX_MINF() : ISO_BASE_COMPOSITE_BOX("minf", NULL) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_BASE_BOX_MINF(uint32_t boxSize, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel) : ISO_BASE_COMPOSITE_BOX("minf", parent, boxSize, boxOffset, boxLevel) {
	}

	virtual ~ISO_BASE_BOX_MINF() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_BASE_COMPOSITE_BOX::GetHeaderSize();
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXMINF_H_ */
