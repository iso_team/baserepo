/*
 * IsoBaseBoxDinf.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXDINF_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXDINF_H_

/// The data reference object contains a table of data references (normally URLs) that declare the location(s) of
/// the media data used within the presentation. The data reference index in the sample description ties entries in
/// this table to the samples in the track. A track
/// may be split over several sources in this way.
/// If the flag is set indicating that the data is in the same file as this box, then no string (not even an empty one)
/// shall be supplied in the entry field.
/// The DataEntryBox within the DataReferenceBox shall be either a DataEntryUrnBox or a DataEntryUrlBox.
struct ISO_BASE_BOX_DINF : public ISO_BASE_COMPOSITE_BOX
{
public:
	/// ctor
	ISO_BASE_BOX_DINF() : ISO_BASE_COMPOSITE_BOX("dinf", NULL) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_BASE_BOX_DINF(uint32_t boxSize, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel) : ISO_BASE_COMPOSITE_BOX("dinf", parent, boxSize, boxOffset, boxLevel) {
	}

	/// dtor
	virtual ~ISO_BASE_BOX_DINF() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_BASE_COMPOSITE_BOX::GetHeaderSize();
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXDINF_H_ */
