/*
 * IsoFullBoxUrn.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXURN_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXURN_H_

/// URN BOX
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_URN : public ISO_FULLBOX
{
	friend class ParseVisitor;
protected:
	std::string m_name;				///< name
	std::string m_location;			///< urn link

public:
	// Note: Space is ok as box names have 4 bytes
	/// ctor
	ISO_FULLBOX_URN() : ISO_FULLBOX("urn ", 0, nullptr) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_URN(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel) : ISO_FULLBOX("urn ", versionAndFlags, parent, boxSize, boxOffset, boxLevel) {
	}

	virtual ~ISO_FULLBOX_URN() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX::GetHeaderSize() + static_cast<uint32_t>(m_location.size() + m_name.size());
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXURN_H_ */
