/*
 * IsoCompositeFullbox.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOCOMPOSITEFULLBOX_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOCOMPOSITEFULLBOX_H_


/// FULL COMPOSITE BOX
/// Composite Fullbox inheriting from Fullbox
/// One can understand this box as a Base box decorated
/// with flags and version and with ability to hold children
struct ISO_COMPOSITE_FULLBOX : public ICOMPOSITE_BOX
{
	friend class ParseVisitor;
protected:
	uint32_t m_versionAndFlags; 		///< decoration with version and flags

public:
	ISO_COMPOSITE_FULLBOX() : ICOMPOSITE_BOX(), m_versionAndFlags(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxName
	/// @param [in] parent
	/// @param [in] boxSize
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_COMPOSITE_FULLBOX(const char* boxName, ICOMPOSITE_BOX* parent, uint32_t boxSize = 0, uint64_t boxOffset = 0, uint32_t boxLevel = 0)
		: ICOMPOSITE_BOX(boxName, parent, boxSize, boxOffset, boxLevel), m_versionAndFlags(0) {
	}

	virtual ~ISO_COMPOSITE_FULLBOX();

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone();

	// Name + Size + Version&Flags
	virtual uint32_t GetHeaderSize() const { return this->ISO_BASE_BOX::GetHeaderSize() + sizeof(m_versionAndFlags); }
};



#endif /* SRC_MAIN_INCLUDE_BOXES_ISOCOMPOSITEFULLBOX_H_ */
