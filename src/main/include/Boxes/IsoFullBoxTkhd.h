/*
 * IsoFullBoxTkhd.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXTKHD_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXTKHD_H_

/// Track Header Box
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_TKHD : public ISO_FULLBOX
{
	friend class ParseVisitor;
protected:
	uint8_t m_reservedStart[8];		///< reserved
	uint16_t m_layer;				///< specifies the front-to-back ordering of video tracks;
	uint16_t m_alternateGroup;		///< ...is an integer that specifies a group or collection of tracks
	uint16_t m_volume;				///< ...is a fixed 8.8 value specifying the track's relative audio volume
	uint8_t m_reservedEnd[38];		///< matrix
	uint32_t m_width;				///< specify the track's visual presentation size as fixed-point 16.16 values
	uint32_t m_height;				///< specify the track's visual presentation size as fixed-point 16.16 values

public:
	/// ctor
	ISO_FULLBOX_TKHD()
		: ISO_FULLBOX("tkhd", 0, nullptr),
		  m_layer(0), m_alternateGroup(0), m_volume(0), m_width(0), m_height(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_TKHD(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("tkhd", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_layer(0), m_alternateGroup(0), m_volume(0), m_width(0), m_height(0) {
	}

	virtual ~ISO_FULLBOX_TKHD() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX::GetHeaderSize() +
			2 * sizeof(uint32_t) + sizeof(m_layer) +
			sizeof(m_alternateGroup) + sizeof(m_volume) + 19 * sizeof(uint16_t) +
			sizeof(m_width) + sizeof(m_height);
	}
};

/// Track Header Box - Version 0 (32bit)
struct ISO_FULLBOX_TKHD_32BIT : public ISO_FULLBOX_TKHD
{
	friend class ParseVisitor;
	friend struct ISO_FULLBOX_TKHD;
protected:
	/// ...is an 32bit integer that declares the creation time of the media in this track
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint32_t m_creationTime;
	/// ...is an 32bit integer that declares the most recent time the media in this track was modified
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint32_t m_modificationTime;
	/// ...is an 32bit integer that uniquely identifies this track over the entire life-time of this presentation.
	/// Track IDs are never re-used and cannot be zero.
	uint32_t m_trackID;
	/// Reserved value 32bit
	uint32_t m_reserved;
	/// ...is an 32bit integer that declares the duration of this media (in the scale of the timescale).
	uint32_t m_duration;
public:
	/// Ctor
	ISO_FULLBOX_TKHD_32BIT()
		: ISO_FULLBOX_TKHD(),
		  m_creationTime(0), m_modificationTime(0), m_trackID(0), m_reserved(0), m_duration(0) {
	}

	/// Parameterized ctor
	/// @param boxSize
	/// @param versionAndFlags
	/// @param parent
	/// @param boxOffset
	/// @param boxLevel
	ISO_FULLBOX_TKHD_32BIT(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX_TKHD(boxSize, versionAndFlags, parent, boxOffset, boxLevel),
		  m_creationTime(0), m_modificationTime(0), m_trackID(0), m_reserved(0), m_duration(0) {
	}

	~ISO_FULLBOX_TKHD_32BIT() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX_TKHD::GetHeaderSize() + sizeof(m_creationTime) + sizeof(m_modificationTime) + sizeof(m_trackID) + sizeof(m_reserved) + sizeof(m_duration);
	}
};

/// Track Header Box - Version 1 (64bit)
struct ISO_FULLBOX_TKHD_64BIT : public ISO_FULLBOX_TKHD
{
	friend class ParseVisitor;
	friend struct ISO_FULLBOX_TKHD;
protected:
	/// ...is an 64bit integer that declares the creation time of the media in this track
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint64_t m_creationTime;
	/// ...is an 64bit integer that declares the most recent time the media in this track was modified
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint64_t m_modificationTime;
	/// ...is an integer that uniquely identifies this track over the entire life-time of this presentation.
	/// Track IDs are never re-used and cannot be zero.
	uint32_t m_trackID;
	/// Reserved value
	uint32_t m_reserved;
	/// ...is an 64bit integer that declares the duration of this media (in the scale of the timescale).
	uint64_t m_duration;
public:
	/// Ctor
	ISO_FULLBOX_TKHD_64BIT()
		: ISO_FULLBOX_TKHD(),
		  m_creationTime(0), m_modificationTime(0), m_trackID(0), m_reserved(0), m_duration(0) {
	}

	/// Parameterized ctor
	/// @param boxSize
	/// @param versionAndFlags
	/// @param parent
	/// @param boxOffset
	/// @param boxLevel
	ISO_FULLBOX_TKHD_64BIT(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX_TKHD(boxSize, versionAndFlags, parent, boxOffset, boxLevel),
		  m_creationTime(0), m_modificationTime(0), m_trackID(0), m_reserved(0), m_duration(0) {
	}

	~ISO_FULLBOX_TKHD_64BIT() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX_TKHD::GetHeaderSize() + sizeof(m_creationTime) + sizeof(m_modificationTime) + sizeof(m_trackID) + sizeof(m_reserved) + sizeof(m_duration);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXTKHD_H_ */
