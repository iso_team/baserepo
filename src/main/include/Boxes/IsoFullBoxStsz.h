/*
 * IsoFullBoxStsz.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSZ_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSZ_H_

/// Sample Size Boxes
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_STSZ : public ISO_FULLBOX
{
	friend class ParseVisitor;
protected:
	/// ...is integer specifying the default sample size. If all the samples are the same size, this field
	/// contains that size value. If this field is set to 0, then the samples have different sizes, and those sizes
	/// are stored in the sample size table.  If this field is not 0, it specifies the constant sample size, and no
	/// array follows.
	uint32_t m_sampleSize;
	/// is an integer that gives the number of samples in the track;  if sample-size is 0, then it is
	/// also the number of entries in the following table.
	uint32_t m_sampleCount;
	/// vector of integers specifying the size of a sample, indexed by its number
	std::vector<uint32_t> m_entrySizes;

public:
	/// ctor
	ISO_FULLBOX_STSZ()
		: ISO_FULLBOX("stsz", 0, nullptr),
		  m_sampleSize(0),
		  m_sampleCount(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_STSZ(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("stsz", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_sampleSize(0),
		  m_sampleCount(0) {
	}

	virtual ~ISO_FULLBOX_STSZ() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return  this->ISO_FULLBOX::GetHeaderSize() + sizeof(m_sampleSize) + sizeof(m_sampleCount) + m_sampleCount * sizeof(uint32_t);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSZ_H_ */
