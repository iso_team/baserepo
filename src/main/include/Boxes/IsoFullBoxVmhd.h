/*
 * IsoFullBoxVmhd.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXVMHD_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXVMHD_H_

#include <array>

/// Video Media Header Box
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_VMHD : public ISO_FULLBOX
{
	friend class ParseVisitor;
protected:
	uint16_t 				m_graphicsMode;		///< specifies a composition mode for this video track
	std::array<uint16_t, 3> m_opColor;			///< is a set of 3 color values (red, green, blue) available for use by graphics modes

public:
	/// ctor
	ISO_FULLBOX_VMHD()
		: ISO_FULLBOX("vmhd", 0, nullptr),
		  m_graphicsMode(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_VMHD(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("vmhd", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_graphicsMode(0) {
	}

	virtual ~ISO_FULLBOX_VMHD() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX::GetHeaderSize() + sizeof(m_graphicsMode) + 3 * sizeof(uint16_t);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXVMHD_H_ */
