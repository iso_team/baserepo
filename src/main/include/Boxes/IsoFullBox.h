/*
 * IsoFullBox.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOX_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOX_H_

/// Fullbox abstraction
/// Represents decorated ISO_BASE_BOX with version and flags
struct ISO_FULLBOX : public ISO_BASE_BOX
{
	friend class ParseVisitor;
protected:
	uint32_t m_versionAndFlags;			///< decoration with version and flags

public:
	/// ctor
	ISO_FULLBOX() : ISO_BASE_BOX(), m_versionAndFlags(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxName
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxSize
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX(const char* boxName, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint32_t boxSize = 0, uint64_t boxOffset = 0, uint32_t boxLevel = 0)
		: ISO_BASE_BOX(boxName, parent, boxSize, boxOffset, boxLevel), m_versionAndFlags(versionAndFlags) {
	}

	virtual ~ISO_FULLBOX() {
	}

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() = 0;

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	// Name + Size + Version&Flags
	virtual uint32_t GetHeaderSize() const { return 3 * sizeof(uint32_t); }
};



#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOX_H_ */
