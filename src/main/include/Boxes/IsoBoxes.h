/*
 * IsoBAseMediaFormatBoxes.h
 *
 *  Created on: Nov 16, 2016
 *      Author: ii01
 */

#pragma once
#include <cstdint>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <memory>
#include "ByteBuffer.h"
#include "Visitors/IVisitor.h"

#include "IsoBaseBox.h"
#include "IsoFullBox.h"
#include "ICompositeBox.h"
#include "IsoBaseCompositeBox.h"
#include "IsoCompositeFullbox.h"
#include "IsoBaseBoxUnknown.h"
#include "IsoBaseBoxFtyp.h"
#include "IsoFullboxMvhd.h"
#include "IsoBaseBoxMoov.h"
#include "IsoBaseBoxTrak.h"
#include "IsoFullBoxTkhd.h"
#include "IsoBaseBoxMdia.h"
#include "IsoFullBoxMdhd.h"
#include "IsoFullBoxHdlr.h"
#include "IsoBaseBoxMinf.h"
#include "IsoFullBoxVmhd.h"
#include "IsoBaseBoxDinf.h"
#include "IsoFullBoxDref.h"
#include "IsoFullBoxUrl.h"
#include "IsoFullBoxUrn.h"
#include "IsoBaseBoxStbl.h"
#include "IsoFullBoxStsd.h"
#include "IsoFullBoxAvc1.h"
#include "IsoBaseBoxAvcc.h"
#include "IsoBaseBoxBtrt.h"
#include "IsoFullBoxStts.h"
#include "IsoFullBoxStss.h"
#include "IsoFullBoxStsc.h"
#include "IsoFullBoxStsz.h"
#include "IsoFullBoxStco.h"
