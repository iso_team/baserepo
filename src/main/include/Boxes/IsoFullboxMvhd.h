/*
 * IsoFullboxMvhd.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXMVHD_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXMVHD_H_

/// Movie Header Box
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_MVHD : public ISO_FULLBOX
{
	friend class ParseVisitor;
protected:
	uint32_t m_rate; 			///< is a fixed point 16.16 number that indicates the preferred rate to play the presentation
	uint16_t m_volume;			///< is a fixed point 8.8 number that indicates the preferred playback volume. 1.0 (0x0100) is full volume
	uint8_t m_reserved[70];		///< matrix reserved
	uint32_t m_nextTrackId;		///< is a non-zero integer that indicates a value to use for the track ID of the next track to be
								///< added to this presentation.  Zero is not a valid track ID value.

public:
	/// ctor
	ISO_FULLBOX_MVHD()
		: ISO_FULLBOX("mvhd", 0, nullptr),
		  m_rate(0), m_volume(0), m_nextTrackId(1) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_MVHD(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("mvhd", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_rate(0), m_volume(0), m_nextTrackId(1) {
	}

	virtual ~ISO_FULLBOX_MVHD() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const { return this->ISO_BASE_BOX::GetHeaderSize() + sizeof(m_rate) +  sizeof(m_volume) + sizeof(m_nextTrackId) + sizeof(uint16_t) * 35; }
};

/// Movie Header Box - version 0 (32bit)
struct ISO_FULLBOX_MVHD_32BIT : public ISO_FULLBOX_MVHD
{
	friend class ParseVisitor;
	friend struct ISO_FULLBOX_MVHD;
protected:
	/// ...is an 32bit integer that declares the creation time of the media in this track
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint32_t m_creationTime;
	/// ...is an 32bit integer that declares the most recent time the media in this track was modified
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint32_t m_modificationTime;
	/// ...is an 32bit integer that specifies the time-scale for this media; this is the number of time units that
	/// pass in one second. For example, a time coordinate system that measures time in sixtieths of a
	/// second has a time scale of 60.
	uint32_t m_timescale;
	/// ...is an 32bit integer that declares the duration of this media (in the scale of the timescale).
	uint32_t m_duration;
public:
	/// Ctor
	ISO_FULLBOX_MVHD_32BIT()
		: ISO_FULLBOX_MVHD(),
		  m_creationTime(0), m_modificationTime(0), m_timescale(0), m_duration(0) {
	}

	/// Parameterized ctor
	/// @param boxSize
	/// @param versionAndFlags
	/// @param parent
	/// @param boxOffset
	/// @param boxLevel
	ISO_FULLBOX_MVHD_32BIT(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX_MVHD(boxSize, versionAndFlags, parent, boxOffset, boxLevel),
		  m_creationTime(0), m_modificationTime(0), m_timescale(0), m_duration(0) {
	}

	~ISO_FULLBOX_MVHD_32BIT() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX_MVHD::GetHeaderSize() + +sizeof(m_creationTime) + sizeof(m_modificationTime) + sizeof(m_timescale) + sizeof(m_duration);
	}
};

/// Movie Header Box - version 1 (64bit)
struct ISO_FULLBOX_MVHD_64BIT : public ISO_FULLBOX_MVHD
{
	friend class ParseVisitor;
	friend struct ISO_FULLBOX_MVHD;
protected:
	/// ...is an 64bit integer that declares the creation time of the media in this track
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint64_t m_creationTime;
	/// ...is an 64bit integer that declares the most recent time the media in this track was modified
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint64_t m_modificationTime;
	/// ...is an 32bit integer that specifies the time-scale for this media; this is the number of time units that
	/// pass in one second. For example, a time coordinate system that measures time in sixtieths of a
	/// second has a time scale of 60.
	uint32_t m_timescale;
	/// ...is an 64bit integer that declares the duration of this media (in the scale of the timescale).
	uint64_t m_duration;
public:
	/// Ctor
	ISO_FULLBOX_MVHD_64BIT()
		: ISO_FULLBOX_MVHD(),
		  m_creationTime(0), m_modificationTime(0), m_timescale(0), m_duration(0) {
	}

	/// Parameterized ctor
	/// @param boxSize
	/// @param versionAndFlags
	/// @param parent
	/// @param boxOffset
	/// @param boxLevel
	ISO_FULLBOX_MVHD_64BIT(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX_MVHD(boxSize, versionAndFlags, parent, boxOffset, boxLevel),
		  m_creationTime(0), m_modificationTime(0), m_timescale(0), m_duration(0) {
	}

	~ISO_FULLBOX_MVHD_64BIT() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX_MVHD::GetHeaderSize() + sizeof(m_creationTime) + sizeof(m_modificationTime) + sizeof(m_timescale) + sizeof(m_duration);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXMVHD_H_ */
