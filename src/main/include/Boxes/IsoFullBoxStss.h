/*
 * IsoFullBoxStss.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSS_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSS_H_

/// Sync Sample Box
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_STSS : public ISO_FULLBOX
{
	friend class ParseVisitor;
protected:
	/// ... is an integer that gives the number of entries in the following table. If entry_count is zero,
	/// there are no random access points within the stream and the following table is empty.
	uint32_t m_entryCount;
	/// vector of numbers that give represent number of the samples that are random access points in the stream
	std::vector<uint32_t> m_stssEntries;

public:
	/// ctor
	ISO_FULLBOX_STSS()
		: ISO_FULLBOX("stss", 0, nullptr),
		  m_entryCount(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_STSS(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("stss", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_entryCount(0) {
	}

	virtual ~ISO_FULLBOX_STSS() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX::GetHeaderSize() + sizeof(m_entryCount) + m_entryCount * sizeof(uint32_t);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSS_H_ */
