/*
 * IsoBaseBoxUnknown.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXUNKNOWN_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXUNKNOWN_H_


/// Unknown Box
/// Box used as a placeholder for all unknown boxes
/// so we can parse the ISO structure even if
/// we do not know all box definitions
struct ISO_BASE_BOX_UNKNOWN : public ISO_BASE_BOX
{
	friend class ParseVisitor;
protected:
	ByteBuffer m_byteBuffer;		///< Payload following box name

public:
	/// ctor
	ISO_BASE_BOX_UNKNOWN() : ISO_BASE_BOX("unkn", NULL) {
	}

	/// Parameterized ctor
	/// @param [in] boxName
	/// @param [in] parent
	/// @param [in] boxSize
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_BASE_BOX_UNKNOWN(std::string boxName, ICOMPOSITE_BOX* parent, uint32_t boxSize, uint64_t boxOffset, uint32_t boxLevel) : ISO_BASE_BOX(boxName.c_str(), parent, boxSize, boxOffset, boxLevel) {
	}

	virtual ~ISO_BASE_BOX_UNKNOWN() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const { return this->ISO_BASE_BOX::GetHeaderSize() + m_byteBuffer.GetSize(); }
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXUNKNOWN_H_ */
