/*
 * IsoFullBoxDref.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXDREF_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXDREF_H_


/// DREF BOX
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_DREF : public ISO_COMPOSITE_FULLBOX
{
	friend class ParseVisitor;
protected:
	uint32_t m_entryCount;			///< Number of entries

public:
	/// ctor
	ISO_FULLBOX_DREF()
		: ISO_COMPOSITE_FULLBOX("dref", NULL),
		  m_entryCount(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_DREF(uint32_t boxSize, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_COMPOSITE_FULLBOX("dref", parent, boxSize, boxOffset, boxLevel),
		  m_entryCount(0) {
	}

	virtual ~ISO_FULLBOX_DREF() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_COMPOSITE_FULLBOX::GetHeaderSize() + sizeof(m_entryCount);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXDREF_H_ */
