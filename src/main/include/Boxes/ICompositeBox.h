/*
 * ICompositeBox.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ICOMPOSITEBOX_H_
#define SRC_MAIN_INCLUDE_BOXES_ICOMPOSITEBOX_H_

#include "IsoBaseBox.h"

////////////////////////////////
/// COMPOSITE BOX INTERFACE ////
////////////////////////////////
struct ICOMPOSITE_BOX : public ISO_BASE_BOX
{
	friend class ParseVisitor;
protected:
	std::vector<std::shared_ptr<ISO_BASE_BOX>> m_children;	///< Vector of shared_ptrs to children

public:
	///
	/// ctor
	///
	ICOMPOSITE_BOX() : ISO_BASE_BOX() {
	}

	///
	/// Parameterized ctor
	/// @param [in] boxName
	/// @param [in] parent
	/// @param [in] boxSize
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	///
	ICOMPOSITE_BOX(const char* boxName, ICOMPOSITE_BOX* parent, uint32_t boxSize = 0, uint64_t boxOffset = 0, uint32_t boxLevel = 0)
		: ISO_BASE_BOX(boxName, parent, boxSize, boxOffset, boxLevel) {
	}

	/// dtor
	virtual ~ICOMPOSITE_BOX() {
	}

	/// Transforms object to a byte buffer
	/// @param sOut
	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	/// Fetches my children
	/// @return@return vector of shared pointers to its children
	std::vector<std::shared_ptr<ISO_BASE_BOX>> GetChildren() { return m_children; }
	/// @return Deep copy of itself
	virtual std::shared_ptr<ISO_BASE_BOX> Clone() = 0;
	/// Add new child
	/// @param sptrChild
	void AddChild(std::shared_ptr<ISO_BASE_BOX> sptrChild);
	/// Input param is raw ptr to a child
	/// @param childPtr
	/// @return shared ptr holding this raw ptr
	std::shared_ptr<ISO_BASE_BOX> GetChildSptr(const ISO_BASE_BOX* const childPtr);
	/// Method used for box swapping
	/// @param child
	/// @param newChild
	/// @return
	std::tuple<int64_t, int64_t, ISO_BASE_BOX*> SwapChild(ISO_BASE_BOX* child, std::shared_ptr<ISO_BASE_BOX> newChild);
	/// Accept visitors implementing IVisitor interface
	/// @param refVisitor
	virtual void Accept(IVisitor& refVisitor) override;
	/// Fetch header size
	virtual uint32_t GetHeaderSize() const = 0;
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ICOMPOSITEBOX_H_ */
