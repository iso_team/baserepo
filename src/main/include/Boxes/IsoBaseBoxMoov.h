/*
 * IsoBaseBoxMoov.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXMOOV_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXMOOV_H_


/// Movie Box
/// For more details consult ISO/IEC::14496-12
struct ISO_BASE_BOX_MOOV : public ISO_BASE_COMPOSITE_BOX
{
public:
	/// ctor
	ISO_BASE_BOX_MOOV() : ISO_BASE_COMPOSITE_BOX("moov", NULL) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_BASE_BOX_MOOV(uint32_t boxSize, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel) : ISO_BASE_COMPOSITE_BOX("moov", parent, boxSize, boxOffset, boxLevel) {
	}

	virtual ~ISO_BASE_BOX_MOOV() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_BASE_COMPOSITE_BOX::GetHeaderSize();
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXMOOV_H_ */
