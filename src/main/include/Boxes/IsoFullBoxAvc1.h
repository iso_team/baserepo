/*
 * IsoFullBoxAvc1.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXAVC1_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXAVC1_H_

#include <array>
/// AVC1 BOX
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_AVC1 : public ISO_COMPOSITE_FULLBOX
{
	friend class ParseVisitor;
protected:
	std::array<uint8_t, 2>			m_reservedStart;			///< reserved
	uint16_t						m_dataRefIndex;				///< data reference index
	std::array<uint8_t, 16>			m_reservedEnd;			///< reserved
	uint16_t						m_width;					///< width
	uint16_t						m_height;					///< height
	uint16_t						m_horRes;					///< horizontal resolution
	uint16_t						m_unknwnHorValue;			///< reserved
	uint16_t						m_vertRes;					///< vertical resolution
	std::array<uint8_t, 40>			m_compressorName;		///< compressor name
	uint16_t						m_colorDepth;				///< color depth
	uint16_t						m_predefined;				///< reserved

public:

	/// ctor
	ISO_FULLBOX_AVC1()
		: ISO_COMPOSITE_FULLBOX("avc1", NULL),
		  m_dataRefIndex(0),
		  m_width(0),
		  m_height(0),
		  m_horRes(0),
		  m_unknwnHorValue(0),
		  m_vertRes(0),
		  m_colorDepth(0),
		  m_predefined(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_AVC1(uint32_t boxSize, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_COMPOSITE_FULLBOX("avc1", parent, boxSize, boxOffset, boxLevel),
		  m_dataRefIndex(0),
		  m_width(0),
		  m_height(0),
		  m_horRes(0),
		  m_unknwnHorValue(0),
		  m_vertRes(0),
		  m_colorDepth(0),
		  m_predefined(0) {
	}

	/// dtor
	virtual ~ISO_FULLBOX_AVC1() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_COMPOSITE_FULLBOX::GetHeaderSize() + 2 * sizeof(uint8_t) +
			sizeof(m_dataRefIndex) + 4*sizeof(uint32_t) +
			sizeof(m_width) + sizeof(m_height) +
			sizeof(m_horRes) + sizeof(m_unknwnHorValue) +
			sizeof(m_vertRes) + 10 * sizeof(uint32_t) +
			sizeof(m_colorDepth) + sizeof(m_predefined);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXAVC1_H_ */
