/*
 * IsoFullBoxStsd.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSD_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSD_H_


///  Sample Description Box
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_STSD : public ISO_COMPOSITE_FULLBOX
{
	friend class ParseVisitor;
protected:
	/// Number of STSD entries
	uint32_t m_numEntries;
public:
	/// ctor
	ISO_FULLBOX_STSD()
		: ISO_COMPOSITE_FULLBOX("stsd", NULL),
		  m_numEntries(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_STSD(uint32_t boxSize, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_COMPOSITE_FULLBOX("stsd", parent, boxSize, boxOffset, boxLevel),
		  m_numEntries(0) {
	}

	virtual ~ISO_FULLBOX_STSD() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_COMPOSITE_FULLBOX::GetHeaderSize() + sizeof(m_numEntries);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSD_H_ */
