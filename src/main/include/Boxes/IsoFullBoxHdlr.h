/*
 * IsoFullBoxHdlr.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXHDLR_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXHDLR_H_


/// HDLR BOX
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_HDLR : public ISO_FULLBOX
{
	friend class ParseVisitor;
protected:
	uint32_t m_predefined;			///< predefined
	uint32_t m_handlerType;			///< when present in a media box can have following values:
									///< 'vide' Video track
									///< 'soun' Audio track
									///< 'hint' Hint track
	uint8_t m_reserved[12];			///< reserved
	std::string m_name;				///< is a null-terminated string in UTF-8 characters which gives a human-readable name for the track type
public:
	/// ctor
	ISO_FULLBOX_HDLR()
		: ISO_FULLBOX("hdlr", 0, nullptr),
		  m_predefined(0), m_handlerType(0), m_name("") {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_HDLR(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("hdlr", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_predefined(0), m_handlerType(0), m_name("") {
	}

	virtual ~ISO_FULLBOX_HDLR() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX::GetHeaderSize() + static_cast<uint32_t>(sizeof(m_predefined) + sizeof(m_handlerType) + 3 * sizeof(uint32_t) + m_name.size());
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXHDLR_H_ */
