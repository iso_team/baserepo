/*
 * IsoFullBoxMdhd.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXMDHD_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXMDHD_H_

/// Media Header Box
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_MDHD : public ISO_FULLBOX
{
	friend class ParseVisitor;
protected:
	/// Declares  the  language  code  for  this  media.  See  ISO  639-2/T  for  the  set  of  three  character
	/// codes. Each character is packed as the difference between its ASCII value and 0x60. Since the code
	/// is confined to being three lower-case letters, these values are strictly positive.
	uint16_t m_language;
	uint16_t m_predefined; ///< predefined
public:
	/// ctor
	ISO_FULLBOX_MDHD()
		: ISO_FULLBOX("mdhd", 0, nullptr),
		  m_language(0), m_predefined(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_MDHD(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("mdhd", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_language(0), m_predefined(0) {
	}

	virtual ~ISO_FULLBOX_MDHD() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone();

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX::GetHeaderSize() + sizeof(m_language) + sizeof(m_predefined);
	}
};

/// Media Header Box - Version 0 (32bit)
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_MDHD_32BIT : public ISO_FULLBOX_MDHD
{
	friend class ParseVisitor;
	friend struct ISO_FULLBOX_MDHD;
protected:
	/// ...is an 32bit integer that declares the creation time of the media in this track
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint32_t m_creationTime;
	/// ...is an 32bit integer that declares the most recent time the media in this track was modified
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint32_t m_modificationTime;
	/// ...is an 32bit integer that specifies the time-scale for this media; this is the number of time units that
	/// pass in one second. For example, a time coordinate system that measures time in sixtieths of a
	/// second has a time scale of 60.
	uint32_t m_timescale;
	/// ...is an 32bit integer that declares the duration of this media (in the scale of the timescale).
	uint32_t m_duration;
public:
	/// ctor
	ISO_FULLBOX_MDHD_32BIT()
		: ISO_FULLBOX_MDHD(),
		  m_creationTime(0), m_modificationTime(0), m_timescale(0), m_duration(0) {
	}

	/// Parameterized ctor
	/// @param boxSize
	/// @param versionAndFlags
	/// @param parent
	/// @param boxOffset
	/// @param boxLevel
	ISO_FULLBOX_MDHD_32BIT(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX_MDHD(boxSize, versionAndFlags, parent, boxOffset, boxLevel),
		  m_creationTime(0), m_modificationTime(0), m_timescale(0), m_duration(0) {
	}

	~ISO_FULLBOX_MDHD_32BIT() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX_MDHD::GetHeaderSize() + sizeof(m_creationTime) + sizeof(m_modificationTime) + sizeof(m_timescale) + sizeof(m_duration);
	}
};

/// Media Header Box - Version 1 (64bit)
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_MDHD_64BIT : public ISO_FULLBOX_MDHD
{
	friend class ParseVisitor;
	friend struct ISO_FULLBOX_MDHD;
protected:
	/// ...is an 64bit integer that declares the creation time of the media in this track
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint64_t m_creationTime;
	/// ...is an 64bit integer that declares the most recent time the media in this track was modified
	/// (in seconds since midnight, Jan. 1, 1904, in UTC time)
	uint64_t m_modificationTime;
	/// ...is an 32bit integer that specifies the time-scale for this media; this is the number of time units that
	/// pass in one second. For example, a time coordinate system that measures time in sixtieths of a
	/// second has a time scale of 60.
	uint32_t m_timescale;
	/// ...is an 64bit integer that declares the duration of this media (in the scale of the timescale).
	uint64_t m_duration;
public:
	/// ctor
	ISO_FULLBOX_MDHD_64BIT()
		: ISO_FULLBOX_MDHD(),
		  m_creationTime(0), m_modificationTime(0), m_timescale(0), m_duration(0){
	}

	/// Parameterized ctor
	/// @param boxSize
	/// @param versionAndFlags
	/// @param parent
	/// @param boxOffset
	/// @param boxLevel
	ISO_FULLBOX_MDHD_64BIT(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX_MDHD(boxSize, versionAndFlags, parent, boxOffset, boxLevel),
		  m_creationTime(0), m_modificationTime(0), m_timescale(0), m_duration(0) {
	}

	~ISO_FULLBOX_MDHD_64BIT() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_FULLBOX_MDHD::GetHeaderSize() + sizeof(m_creationTime) + sizeof(m_modificationTime) + sizeof(m_timescale) + sizeof(m_duration);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXMDHD_H_ */
