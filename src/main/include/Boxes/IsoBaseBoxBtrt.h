/*
 * IsoBaseBoxBtrt.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXBTRT_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXBTRT_H_


/// Bitrate box -> Used for AVC file format
struct ISO_BASE_BOX_BTRT : public ISO_BASE_BOX
{
	friend class ParseVisitor;
protected:
	uint32_t m_bufferSize;		///< buffer size
	uint32_t m_maxBitrate;		///< max bitrate
	uint32_t m_minBitrate;		///< min bitrate

public:

	/// ctor
	ISO_BASE_BOX_BTRT()
		: ISO_BASE_BOX("btrt", NULL),
		  m_bufferSize(0), m_maxBitrate(0), m_minBitrate(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_BASE_BOX_BTRT(uint32_t boxSize, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_BASE_BOX("btrt", parent, boxSize, boxOffset, boxLevel),
		  m_bufferSize(0), m_maxBitrate(0), m_minBitrate(0) {
	}

	/// dtor
	virtual ~ISO_BASE_BOX_BTRT() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return this->ISO_BASE_BOX::GetHeaderSize() + sizeof(m_bufferSize) + sizeof(m_maxBitrate) + sizeof(m_minBitrate);
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXBTRT_H_ */
