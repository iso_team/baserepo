/*
 * IsoBaseBox.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOBASEBOX_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOBASEBOX_H_

#include <cstdint>
#include <string>
#include <memory>
#include "Visitors/IVisitor.h"
#include "ByteBuffer.h"

struct ICOMPOSITE_BOX;

//////////////////////
/// BASE ISO BOX /////
//////////////////////
struct ISO_BASE_BOX
{
	friend class ParseVisitor;
protected:
	uint32_t		m_boxSize;			///< Box size
	std::string		m_boxName;			///< Box name
	uint64_t		m_boxOffset;		///< Box offset (relative to the start of a file)
	uint32_t		m_boxLevel;			///< Box level within the IsoTree
	ICOMPOSITE_BOX*	m_parent;			///< Keeps track of aparent node

public:
	/// Default ctor
	ISO_BASE_BOX() : m_boxSize(0), m_boxOffset(0), m_boxLevel(0), m_parent(nullptr)  {
	}

	/// Parameterized ctor
	/// @param [in] boxName
	/// @param [in] parent
	/// @param [in] boxSize
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_BASE_BOX(const char* boxName, ICOMPOSITE_BOX* parent, uint32_t boxSize = 0, uint64_t boxOffset = 0, uint32_t boxLevel = 0)
		: m_boxSize(boxSize),
		  m_boxName(boxName),
		  m_boxOffset(boxOffset),
		  m_boxLevel(boxLevel),
		  m_parent(parent) {
	}

	/// default dtor
	virtual ~ISO_BASE_BOX() {
	}

	/// @return Box size
	uint32_t GetSize() const { return this->m_boxSize; }

	/// Box setter
	/// @param boxSize
	void SetBoxSize(uint32_t boxSize) { this->m_boxSize = boxSize; }

	/// @return Header size (Box Name + Box Size)
	virtual uint32_t GetHeaderSize() const { return 2 * sizeof(uint32_t); }

	/// @return Box name
	std::string GetBoxName() const { return this->m_boxName; }

	/// @return Box offset
	uint64_t GetStartOffset() const { return this->m_boxOffset; }

	/// @param boxOffset
	void SetBoxOffset(uint64_t boxOffset) { this->m_boxOffset = boxOffset; }

	/// @return Box level
	uint32_t GetBoxLevel() const { return this->m_boxLevel; }

	/// @return Ptr parent box
	ICOMPOSITE_BOX* GetParentBox() const { return this->m_parent; }

	/// Parent box setter
	/// @param parent
	void SetParentBox(ICOMPOSITE_BOX* parent) { this->m_parent = parent; }

	/// Returns self shared pointer
	/// @return shared_ptr<this>
	std::shared_ptr<ISO_BASE_BOX> GetSelfSptr() const;
	/// @return Deep copy of a box
	virtual std::shared_ptr<ISO_BASE_BOX> Clone() = 0;
	/// Transforms object to a byte buffer
	///  @param sOut
	virtual void ToByteBuffer(ByteBuffer& sOut) const;
	/// Prints out basic info about box
	virtual void Print() const;
	/// Interface for various visitor implementations
	/// @param refVisitor
	virtual void Accept(IVisitor& refVisitor);
};


#endif /* SRC_MAIN_INCLUDE_BOXES_ISOBASEBOX_H_ */
