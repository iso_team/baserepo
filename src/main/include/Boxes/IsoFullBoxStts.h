/*
 * IsoFullBoxStts.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTTS_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTTS_H_

/// Decoding Time to Sample Box
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_STTS : public ISO_FULLBOX
{
	friend class ParseVisitor;
private:
	struct STTS_ENTRY {
		uint32_t m_sampleCount;
		uint32_t m_sampleDelta;
	};

protected:
	/// is an integer that gives the number of entries in the following vector.
	uint32_t m_entryCount;
	/// vector of STTS_ENTRY pair where
	/// sample_count - is an integer that counts the number of consecutive samples that have the given duration.
	/// sample_delta - is an integer that gives the delta of these samples in the time-scale of the media.
	std::vector<STTS_ENTRY> m_sttsEntries;

public:
	/// ctor
	ISO_FULLBOX_STTS()
		: ISO_FULLBOX("stts", 0, nullptr),
		  m_entryCount(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_STTS(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("stts", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_entryCount(0) {
	}

	virtual ~ISO_FULLBOX_STTS() {
	}

	/// Utility allocation while parsing the structure
	inline void AllocateEntries(uint32_t count){
		this->m_sttsEntries.assign(count, STTS_ENTRY());
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		uint32_t headerSize = this->ISO_FULLBOX::GetHeaderSize() + sizeof(m_entryCount);

		for (uint32_t i = 0; i < m_entryCount; i++) {
			headerSize += sizeof(m_sttsEntries[i].m_sampleCount) + sizeof(m_sttsEntries[i].m_sampleDelta);
		}

		return headerSize;
	}
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTTS_H_ */
