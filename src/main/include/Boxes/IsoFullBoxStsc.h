/*
 * IsoFullBoxStsc.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSC_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSC_H_

/// Sample To Chunk Box
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_STSC : public ISO_FULLBOX
{
	friend class ParseVisitor;
private:
	/// STSC_ENTRY block
	struct STSC_ENTRY {
		/// ... is an integer that gives the index of the first chunk in this run of chunks that share the
		/// same samples-per-chunk and sample-description-index; the index of the first chunk in a track has the
		/// value 1 (the first_chunk field in the first record of this box has the value 1, identifying that the first
		/// sample maps to the first chunk).
		uint32_t m_firstChunk;
		/// ... is an integer that gives the number of samples in each of these chunks
		uint32_t m_samplesPerChunk;
		/// ... is an integer that gives the index of the sample entry that describes the
		/// samples in this chunk. The index ranges from 1 to the number of
		/// sample entries in the Sample Description Box
		uint32_t m_sampleDescIndex;
	};
protected:
	/// ...is an integer that gives the number of entries in the following table
	uint32_t m_entryCount;
	/// vector of STSC_ENTRY blocks
	std::vector<STSC_ENTRY> m_stscEntries;

public:
	/// ctor
	ISO_FULLBOX_STSC()
		: ISO_FULLBOX("stsc", 0, nullptr),
		m_entryCount(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_STSC(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("stsc", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_entryCount(0) {
	}

	virtual ~ISO_FULLBOX_STSC() {
	}

	/// Utility allocation while parsing the structure
	inline void AllocateEntries(uint32_t count){
		this->m_stscEntries.assign(count, STSC_ENTRY());
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		uint32_t headerSize = this->ISO_FULLBOX::GetHeaderSize() + sizeof(m_entryCount);

		for (uint32_t i = 0; i < m_entryCount; i++) {
			headerSize += sizeof(m_stscEntries[i].m_firstChunk) + sizeof(m_stscEntries[i].m_samplesPerChunk) + sizeof(m_stscEntries[i].m_sampleDescIndex);
		}

		return headerSize;
	}
};



#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTSC_H_ */
