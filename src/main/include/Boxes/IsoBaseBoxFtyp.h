/*
 * IsoBaseBoxFtyp.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXFTYP_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXFTYP_H_


/// File type box
/// For more details consult ISO/IEC::14496-12
struct ISO_BASE_BOX_FTYP : public ISO_BASE_BOX
{
	friend class ParseVisitor;
protected:
	uint32_t m_majorVersion;						///< major brand
	uint32_t m_minorVersion;						///< minor version
	std::vector<uint32_t> m_compatibleBrands;		///< compatible brands

public:
	/// ctor
	ISO_BASE_BOX_FTYP() : ISO_BASE_BOX("ftyp", NULL), m_majorVersion(0), m_minorVersion(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_BASE_BOX_FTYP(uint32_t boxSize, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel) : ISO_BASE_BOX("ftyp", parent, boxSize, boxOffset, boxLevel), m_majorVersion(0), m_minorVersion(0) {
	}

	virtual ~ISO_BASE_BOX_FTYP() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const { return this->ISO_BASE_BOX::GetHeaderSize() + static_cast<uint32_t>(sizeof(m_majorVersion) * (m_compatibleBrands.size() + 2)); }
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXFTYP_H_ */
