/*
 * IsoBaseCompositeBox.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOBASECOMPOSITEBOX_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOBASECOMPOSITEBOX_H_
#include "ICompositeBox.h"

/// BASE COMPOSITE BOX
/// Composite box inheriting from Base Box
/// Represents abstraction of a box that can have children boxes
struct ISO_BASE_COMPOSITE_BOX : public ICOMPOSITE_BOX
{
	friend class ParseVisitor;
public:
	/// ctor
	ISO_BASE_COMPOSITE_BOX() : ICOMPOSITE_BOX() {
	}

	/// Parameterized ctor
	/// @param [in] boxName
	/// @param [in] parent
	/// @param [in] boxSize
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_BASE_COMPOSITE_BOX(const char* boxName, ICOMPOSITE_BOX* parent, uint32_t boxSize = 0, uint64_t boxOffset = 0, uint32_t boxLevel = 0)
		: ICOMPOSITE_BOX(boxName, parent, boxSize, boxOffset, boxLevel) {
	}

	virtual ~ISO_BASE_COMPOSITE_BOX();

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	// Name + Size
	virtual uint32_t GetHeaderSize() const { return this->ISO_BASE_BOX::GetHeaderSize(); }
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOBASECOMPOSITEBOX_H_ */
