/*
 * IsoFullBoxStco.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTCO_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTCO_H_

/// Chunk Offset Box
/// For more details consult ISO/IEC::14496-12
struct ISO_FULLBOX_STCO : public ISO_FULLBOX
{
	friend class ParseVisitor;
protected:
	uint32_t m_entryCount;					///< ...is an integer that gives the number of entries in the following table
	std::vector<uint32_t> m_chunkOffsets;	///< vector of: 32 or 64 bit integers that give the offset of the start of a
	 	 	 	 	 	 	 	 	 	 	///< chunk into its containing media file.

public:
	/// ctor
	ISO_FULLBOX_STCO()
		: ISO_FULLBOX("stco", 0, nullptr),
		  m_entryCount(0){
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] versionAndFlags
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_FULLBOX_STCO(uint32_t boxSize, uint32_t versionAndFlags, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_FULLBOX("stco", versionAndFlags, parent, boxSize, boxOffset, boxLevel),
		  m_entryCount(0) {
	}

	virtual ~ISO_FULLBOX_STCO() {
	}

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const {
		return  this->ISO_FULLBOX::GetHeaderSize() + sizeof(m_entryCount) + m_entryCount * sizeof(uint32_t);
	}

	void UpdateOffsets(uint32_t mdatOffset);
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOFULLBOXSTCO_H_ */
