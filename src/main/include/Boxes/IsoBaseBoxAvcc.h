/*
 * IsoBaseBoxAvcc.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXAVCC_H_
#define SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXAVCC_H_

#include "IsoBaseBox.h"

/// AVCC BOX -> used for H264 codec
struct ISO_BASE_BOX_AVCC : public ISO_BASE_BOX
{
	friend class ParseVisitor;
protected:
	uint8_t m_configVersion;					///< Configuration version
	uint8_t m_avcProfileIndication;				///< AVC profile
	uint8_t m_profileCompatibility;				///< AVC profile compatibility
	uint8_t m_avcLevelIndication;				///< AVC level
	uint8_t m_length;							///< Length
	uint8_t m_spsCount;							///< SPS count
	std::vector<ByteBuffer*> m_spsEntries;		///< SPS entries
	uint8_t m_ppsCount;							///< PPS count
	std::vector<ByteBuffer*> m_ppsEntries;		///< PPS entries

public:
	/// ctor
	ISO_BASE_BOX_AVCC()
		: ISO_BASE_BOX("avcC", NULL),
		  m_configVersion(0), m_avcProfileIndication(0), m_profileCompatibility(0),
		  m_avcLevelIndication(0), m_length(0), m_spsCount(0), m_ppsCount(0) {
	}

	/// Parameterized ctor
	/// @param [in] boxSize
	/// @param [in] parent
	/// @param [in] boxOffset
	/// @param [in] boxLevel
	ISO_BASE_BOX_AVCC(uint32_t boxSize, ICOMPOSITE_BOX* parent, uint64_t boxOffset, uint32_t boxLevel)
		: ISO_BASE_BOX("avcC", parent, boxSize, boxOffset, boxLevel),
		  m_configVersion(0), m_avcProfileIndication(0), m_profileCompatibility(0),
		  m_avcLevelIndication(0), m_length(0), m_spsCount(0), m_ppsCount(0){
	}

	virtual ~ISO_BASE_BOX_AVCC();

	virtual void ToByteBuffer(ByteBuffer& sOut) const;

	virtual void Accept(IVisitor& refVisitor) override;

	virtual std::shared_ptr<ISO_BASE_BOX> Clone() override;

	virtual uint32_t GetHeaderSize() const;
};

#endif /* SRC_MAIN_INCLUDE_BOXES_ISOBASEBOXAVCC_H_ */
