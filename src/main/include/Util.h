/*
 * Util.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_UTIL_H_
#define SRC_MAIN_INCLUDE_UTIL_H_

#pragma once
#include <cstdint>

/// Set of utility methods used for parsing, processing and similar operations
class Util
{
public:
	/// @param [in] dataPtr Source that shall be read
	/// @param [in,out] value Parsing will be made to value based on its length (sizeof(T))
	template<typename T>
	static void ReadBytesToInt(unsigned char* dataPtr, T& value) {
		ReadBytesToInt((char*)dataPtr, value);
	}
	/// @param [in] dataPtr Source that shall be read
	/// @param [in,out] value Parsing will be made to value based on its length (sizeof(T))
	template<typename T>
	static void ReadBytesToInt(char* dataPtr, T& value) {
		value = 0;
		size_t shifter = 0;
		T temp;
		for (int i = sizeof(T) - 1; i >= 0; i--) {
			uint8_t bla = (dataPtr + i)[0];
			temp = bla;
			temp <<= shifter;
			value += temp;
			shifter += 8;
		}
	}

	/// Int to byte array
	/// @param [in,out] dataPtr Destination where integer value will be written
	/// @param [in] value Integer to convert to bytes and store to dataPtr
	template<typename T>
	static void WriteIntToBytes(unsigned char* dataPtr, T& value) {
		WriteIntToBytes((char*)dataPtr, value);
	}

	/// Int to char array
	/// @param [in,out] dataPtr Destination where integer value will be written
	/// @param [in] value Integer to convert to bytes and store to dataPtr
	template<typename T>
	static void WriteIntToBytes(char* dataPtr, T& value) {
		for (int i = sizeof(T) - 1; i >= 0; i--) {
			dataPtr[sizeof(T) - i - 1] = (value >> 8 * i) & 0xFF;
		}
	}

	/// Endianess swapping method
	/// @param [in] value
	/// @return swapped value
	template<typename T>
	static T SwapEndians(T value) {
		size_t shifter = 0;
		size_t antiShifter = (sizeof(T) - 1) * 8;
		T temp;
		T returnValue = 0;
		for (int i = sizeof(T) - 1; i >= 0; i--) {
			uint8_t bla = (value >> shifter) & 0xFF;
			temp = bla;
			temp <<= shifter;
			returnValue += temp;
			shifter += 8;
			antiShifter -= 8;
		}

		return returnValue;
	}

	/// @param [in] ptr Pointer to a template argument to delete
	template<typename T>
	static inline void delete_ptr(T* ptr) {
		if (ptr != nullptr) {
			delete ptr;
			ptr = nullptr;
		}
	}

	/// Tokenization of input string
	/// @param [in] str Input string that shall be tokenized
	/// @param [in] c Delimiter character used in tokenization process
	/// @param [out] output Vector containing token strings
	static void StringSplit(const char *str, char c, std::vector<std::string>& output) {
		do {
			const char *begin = str;

			while (*str != c && *str){
				str++;
			}

			output.push_back(std::string(begin, str));
		} while (0 != *str++);
	}
};

#endif /* SRC_MAIN_INCLUDE_UTIL_H_ */
