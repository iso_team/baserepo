/*
 * IsoTree.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_ISOTREE_H_
#define SRC_MAIN_INCLUDE_ISOTREE_H_

#include <vector>
#include <memory>
#include "Boxes/IsoBaseCompositeBox.h"
#include "Boxes/IsoBaseBox.h"

/// Main structure of a ISO base media file is kept in this class
/// IsoTree provides various interfaces for box lookup, print, convert to string
class IsoTree
{
private:
	std::shared_ptr<ISO_BASE_COMPOSITE_BOX> m_root;

public:
	IsoTree();

	void VisitTree(IVisitor& v);

	std::vector<std::shared_ptr<ISO_BASE_BOX>> FindBoxChain(std::vector<std::string> boxNames);

	std::vector<std::shared_ptr<ISO_BASE_BOX>> FindBox(std::string boxName);

	void Print() const;

	void Parse(ByteBuffer& byteBuffer);

	void ToByteBuffer(ByteBuffer& byteBuffer) const;
};



#endif /* SRC_MAIN_INCLUDE_ISOTREE_H_ */
