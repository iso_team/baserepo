/*
 * IsoBaseMediaFormatParser.h
 *
 *  Created on: Nov 16, 2016
 *      Author: root
 */

#ifndef SRC_MAIN_INCLUDE_ISOPARSER_H_
#define SRC_MAIN_INCLUDE_ISOPARSER_H_

#include <memory>
#include "Boxes/IsoBoxes.h"
#include "IsoTree.h"

///
/// IsoParser is a static class used for transforming ISO base media file (mp4, 3gp, etc)
/// to a IsoTree structure which represent ISO hierarchy described in
/// ISO/IEC 14496-12 - MPEG-4 Part 12
class IsoParser {
public:
	static std::shared_ptr<IsoTree> Parse(const char* filePath);
};

#endif /* SRC_MAIN_INCLUDE_ISOPARSER_H_ */
