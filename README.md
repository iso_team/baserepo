ISO BASE MEDIA PARSER

### About ###
This is a C++11 x64 library for ISO base media file format (and extensions) parsing.
Extensions like MP4 and 3GP can also be processed.

### How do I get set up? ###
Setup is straight forward...
Use Eclipse CDT project for Unix OS or MSVS 2013+ for Windows OS.
There is currently no support for MacOS.

### Contribution guidelines ###
TODO:

### Repo Info ###
Owner: Ivo Ivanisevic
Linkedin: https://hr.linkedin.com/in/ivo-ivanišević-937a5531